<?php

use Illuminate\Support\Facades\Route;
use Tymon\JWTAuth\Contracts\Providers\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/setting', function () {
    return view('main.settings');
});

//KIDS
Route::prefix("kids")->group(function () {
    Route::get('/', function () {
        return view('main.kids.books');
    });
    Route::get('book/{id}', function ($id) {
        $data["book_id"] = $id;
        return view('main.kids.pages', $data);
    });

    Route::get('book/page/{id}', function ($id) {
        $data["page_id"] = $id;
        return view('main.kids.sounds', $data);
    });

    Route::get('bonus', function () {
        return view('main.kids.bonus');
    });

    Route::get('setting', function () {
        return view('main.kids.kids-settings');
    });
});

Route::prefix("psbs")->group(function () {
    Route::get('/', function () {
        return view('main.psbs.books');
    });
    Route::get('book/{id}', function ($id) {
        $data["book_id"] = $id;
        return view('main.psbs.pages', $data);
    });

    Route::get('book/page/{id}', function ($id) {
        $data["page_id"] = $id;
        return view('main.psbs.sounds', $data);
    });


    Route::get('setting', function () {
        return view('main.psbs.psbs-settings');
    });
});

//END OF KIDS



Route::prefix('employee')->group(function () {
	Route::get('/add', function () {
	    return view('employee.add-employee');
	});
    Route::get('/profile/{id}', function ($id) {
        $data["user_id"] = $id;
        return view('employee.add-employee', $data);
    });
});
