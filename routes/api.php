<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'App\Http\Controllers\api\Auth\AuthController@login');
Route::post('logout', 'App\Http\Controllers\api\Auth\AuthController@logout');
Route::get('profile', 'App\Http\Controllers\api\Auth\AuthController@me');
Route::post('cek', 'App\Http\Controllers\api\Auth\AuthController@cek');


Route::prefix("master")->group(function () {
    Route::get("/roles", 'App\Http\Controllers\Controller@getAllRoles');
    Route::get("/rules", 'App\Http\Controllers\Controller@getRules');
    Route::post("/rules/edit", 'App\Http\Controllers\Controller@editRules');
});

Route::prefix("admin")->group(function () {
    Route::get("/check", 'App\Http\Controllers\Controller@check');

    Route::prefix("/setting")->group(function () {
        Route::post("/change-auth", 'App\Http\Controllers\api\Controller@changeUserPass');

    });

    Route::get("/me", 'App\Http\Controllers\Controller@me');
    Route::post("/change-username", 'App\Http\Controllers\Controller@changeUsername');
    Route::post("/change-password", 'App\Http\Controllers\Controller@changeUserPass');
    Route::prefix("kids")->group(function () {
        Route::prefix("book")->group(function () {
            Route::post("/create", 'App\Http\Controllers\api\kids\BookController@createBook');
            Route::post("/update/{id}", 'App\Http\Controllers\api\kids\BookController@updateBook')->where('id', '[0-9]+');
            Route::delete("/delete/{id}", 'App\Http\Controllers\api\kids\BookController@deleteBook')->where('id', '[0-9]+');
            Route::prefix("/page")->group(function () {
                Route::post("/create", 'App\Http\Controllers\api\kids\BookController@createPage');
                Route::post("/update/{id}", 'App\Http\Controllers\api\kids\BookController@updatePage')->where('id', '[0-9]+');
                Route::delete("/delete/{id}", 'App\Http\Controllers\api\kids\BookController@deletePage')->where('id', '[0-9]+');
                Route::prefix("/sound")->group(function () {
                    Route::post("/create", 'App\Http\Controllers\api\kids\BookController@createSound');
                    Route::post("/update/{id}", 'App\Http\Controllers\api\kids\BookController@updateSound')->where('id', '[0-9]+');
                    Route::delete("/delete/{id}", 'App\Http\Controllers\api\kids\BookController@deleteSound')->where('id', '[0-9]+');
                });
            });
        });
        Route::prefix("information")->group(function () {
            Route::post("/about", 'App\Http\Controllers\api\kids\MasterController@setAbout');
            Route::post("/guide", 'App\Http\Controllers\api\kids\MasterController@setGuide');
        });
        Route::prefix("bonus")->group(function () {
            Route::post("/create", 'App\Http\Controllers\api\kids\BookController@createBonusSound');
            Route::post("/update/{id}", 'App\Http\Controllers\api\kids\BookController@updateBonusSound');
            Route::delete("/delete/{id}", 'App\Http\Controllers\api\kids\BookController@deleteBonusSound')->where('id', '[0-9]+');
        });
    });
    Route::prefix("psbs")->group(function () {
        Route::prefix("book")->group(function () {
            Route::post("/create", 'App\Http\Controllers\api\psbs\BookController@createBook');
            Route::post("/update/{id}", 'App\Http\Controllers\api\psbs\BookController@updateBook')->where('id', '[0-9]+');
            Route::delete("/delete/{id}", 'App\Http\Controllers\api\psbs\BookController@deleteBook')->where('id', '[0-9]+');
            Route::prefix("/page")->group(function () {
                Route::post("/create", 'App\Http\Controllers\api\psbs\BookController@createPage');
                Route::post("/update/{id}", 'App\Http\Controllers\api\psbs\BookController@updatePage')->where('id', '[0-9]+');
                Route::delete("/delete/{id}", 'App\Http\Controllers\api\psbs\BookController@deletePage')->where('id', '[0-9]+');
                Route::prefix("/sound")->group(function () {
                    Route::post("/create", 'App\Http\Controllers\api\psbs\BookController@createSound');
                    Route::post("/update/{id}", 'App\Http\Controllers\api\psbs\BookController@updateSound')->where('id', '[0-9]+');
                    Route::delete("/delete/{id}", 'App\Http\Controllers\api\psbs\BookController@deleteSound')->where('id', '[0-9]+');
                });
            });
        });
        Route::prefix("information")->group(function () {
            Route::post("/about", 'App\Http\Controllers\api\psbs\MasterController@setAbout');
            Route::post("/guide", 'App\Http\Controllers\api\psbs\MasterController@setGuide');
        });
    });
});

//PUBLIC

Route::prefix("kids")->group(function () {
    Route::get("/information", 'App\Http\Controllers\api\kids\MasterController@getInformation');
    Route::prefix("book")->group(function () {
        Route::get("/all", 'App\Http\Controllers\api\kids\BookController@getAllBooks');
        Route::get("/detail/{id}", 'App\Http\Controllers\api\kids\BookController@getBookDetail');
        Route::get("/page/detail/{id}", 'App\Http\Controllers\api\kids\BookController@getPageDetail')->where('id', '[0-9]+');
        Route::get("/code/{code}", 'App\Http\Controllers\api\kids\BookController@getBookByCode');
    });
    Route::prefix("bonus")->group(function () {
        Route::get("/all", 'App\Http\Controllers\api\kids\BookController@getAllBonusSounds');
    });
});

Route::prefix("psbs")->group(function () {
    Route::get("/information", 'App\Http\Controllers\api\psbs\MasterController@getInformation');
    Route::prefix("book")->group(function () {
        Route::get("/all", 'App\Http\Controllers\api\psbs\BookController@getAllBooks');
        Route::get("/detail/{id}", 'App\Http\Controllers\api\psbs\BookController@getBookDetail');
        Route::get("/page/detail/{id}", 'App\Http\Controllers\api\psbs\BookController@getPageDetail')->where('id', '[0-9]+');
        Route::get("/code/{code}", 'App\Http\Controllers\api\psbs\BookController@getBookByCode');
    });
});



