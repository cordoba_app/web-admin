--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: plpgsql
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: _kids_master_book; Type: TABLE; Schema: public; Owner: plpgsql
--


DROP TABLE IF EXISTS
    public.tbl_user,
    public.master_role,
    public.kids_master_book,
    public.kids_tbl_page,
    public.kids_tbl_sound;

CREATE TABLE public.kids_master_book (
    id SERIAL PRIMARY KEY,
    name character varying(32) DEFAULT NULL::character varying,
    cover_url character varying(200) DEFAULT NULL::character varying,
    cover_sound_url character varying(200) DEFAULT NULL::character varying,
    number smallint,
    code character varying(11) DEFAULT NULL::character varying,
    prepage smallint,
    created_at character varying(19) DEFAULT NULL::character varying,
    updated_at character varying(19) DEFAULT NULL::character varying
);


ALTER TABLE public.kids_master_book OWNER TO plpgsql;

--
-- Name: _kids_tbl_page; Type: TABLE; Schema: public; Owner: plpgsql
--

CREATE TABLE public.kids_tbl_page (
    id SERIAL PRIMARY KEY,
    book_id smallint,
    page_number smallint,
    image_url character varying(200) DEFAULT NULL::character varying,
    created_at character varying(19) DEFAULT NULL::character varying,
    updated_at character varying(19) DEFAULT NULL::character varying
);


ALTER TABLE public.kids_tbl_page OWNER TO plpgsql;

--
-- Name: _kids_tbl_sound; Type: TABLE; Schema: public; Owner: plpgsql
--

CREATE TABLE public.kids_tbl_sound (
    id SERIAL PRIMARY KEY,
    page_id smallint,
    orders character varying(1) DEFAULT NULL::character varying,
    sound_url character varying(100) DEFAULT NULL::character varying,
    type smallint,
    created_at character varying(20) DEFAULT NULL::character varying,
    updated_at character varying(20) DEFAULT NULL::character varying
);


ALTER TABLE public.kids_tbl_sound OWNER TO plpgsql;

--
-- Name: _master_role; Type: TABLE; Schema: public; Owner: plpgsql
--

CREATE TABLE public.master_role (
    id SERIAL PRIMARY KEY,
    level smallint,
    name character varying(10) DEFAULT NULL::character varying,
    created_at character varying(19) DEFAULT NULL::character varying,
    updated_at character varying(19) DEFAULT NULL::character varying
);


ALTER TABLE public.master_role OWNER TO plpgsql;

--
-- Name: _tbl_user; Type: TABLE; Schema: public; Owner: plpgsql
--

CREATE TABLE public.tbl_user (
    id SERIAL PRIMARY KEY,
    name character varying(10) DEFAULT NULL::character varying,
    role_id smallint,
    gender smallint,
    phone character varying(1) DEFAULT NULL::character varying,
    username character varying(6) DEFAULT NULL::character varying,
    password character varying(60) DEFAULT NULL::character varying,
    created_at character varying(19) DEFAULT NULL::character varying,
    updated_at character varying(19) DEFAULT NULL::character varying
);


ALTER TABLE public.tbl_user OWNER TO plpgsql;


--
-- Data for Name: _kids_master_book; Type: TABLE DATA; Schema: public; Owner: plpgsql
--

COPY public.kids_master_book (id, name, cover_url, cover_sound_url, number, code, prepage, created_at, updated_at) FROM stdin;
1	Ajarkan Aku Cinta Sholat	upload/books/cover/84d9313e91bbef1151739930b36af172.jpg	upload/books/sound/84d9313e91bbef1151739930b36af172.mp3	1	kids-book-1	2	2022-02-03 02:47:05	2022-02-16 05:47:13
2	Ajarkan Aku Cinta Puasa	upload/books/cover/84d9313e91bbef1151739930b36af172.jpg	upload/books/sound/84d9313e91bbef1151739930b36af172.mp3	2	kids-book-2	2	2022-02-03 02:47:05	2022-02-16 05:47:13
3	Ajarkan Aku Cinta Zakat	upload/books/cover/84d9313e91bbef1151739930b36af172.jpg	upload/books/sound/84d9313e91bbef1151739930b36af172.mp3	3	kids-book-3	2	2022-02-03 02:47:05	2022-02-16 05:47:13
4	Ajarkan Aku Cinta Haji dan Umroh	upload/books/cover/84d9313e91bbef1151739930b36af172.jpg	upload/books/sound/84d9313e91bbef1151739930b36af172.mp3	4	kids-book-4	2	2022-02-03 02:47:05	2022-02-16 05:47:13
5	Ajarkan Aku Cinta Mengaji	upload/books/cover/84d9313e91bbef1151739930b36af172.jpg	upload/books/sound/84d9313e91bbef1151739930b36af172.mp3	5	kids-book-5	2	2022-02-03 02:47:05	2022-02-16 05:47:13
\.


--
-- Data for Name: _kids_tbl_page; Type: TABLE DATA; Schema: public; Owner: plpgsql
--

COPY public.kids_tbl_page (id, book_id, page_number, image_url, created_at, updated_at) FROM stdin;
1	1	1	upload/books/pages/1/Shalat_hal_0b.jpg	2/16/2022 6:06	2/16/2022 6:06
2	1	2	upload/books/pages/1/Shalat_hal_0c.jpg	2/17/2022 6:06	2/17/2022 6:06
3	1	3	upload/books/pages/1/Shalat_hal_1.jpg	2/18/2022 6:06	2/18/2022 6:06
4	1	4	upload/books/pages/1/Shalat_hal_2.jpg	2/19/2022 6:06	2/19/2022 6:06
5	1	5	upload/books/pages/1/Shalat_hal_3.jpg	2/20/2022 6:06	2/20/2022 6:06
6	1	6	upload/books/pages/1/Shalat_hal_4.jpg	2/21/2022 6:06	2/21/2022 6:06
7	1	7	upload/books/pages/1/Shalat_hal_5.jpg	2/22/2022 6:06	2/22/2022 6:06
8	1	8	upload/books/pages/1/Shalat_hal_6.jpg	2/23/2022 6:06	2/23/2022 6:06
9	1	9	upload/books/pages/1/Shalat_hal_7.jpg	2/24/2022 6:06	2/24/2022 6:06
10	1	10	upload/books/pages/1/Shalat_hal_8.jpg	2/25/2022 6:06	2/25/2022 6:06
11	1	11	upload/books/pages/1/Shalat_hal_9.jpg	2/26/2022 6:06	2/26/2022 6:06
12	1	12	upload/books/pages/1/Shalat_hal_10.jpg	2/27/2022 6:06	2/27/2022 6:06
13	1	13	upload/books/pages/1/Shalat_hal_11.jpg	2/28/2022 6:06	2/28/2022 6:06
14	1	14	upload/books/pages/1/Shalat_hal_12.jpg	3/1/2022 6:06	3/1/2022 6:06
15	1	15	upload/books/pages/1/Shalat_hal_13.jpg	3/2/2022 6:06	3/2/2022 6:06
16	1	16	upload/books/pages/1/Shalat_hal_14.jpg	3/3/2022 6:06	3/3/2022 6:06
17	1	17	upload/books/pages/1/Shalat_hal_15.jpg	3/4/2022 6:06	3/4/2022 6:06
18	1	18	upload/books/pages/1/Shalat_hal_16.jpg	3/5/2022 6:06	3/5/2022 6:06
19	1	19	upload/books/pages/1/Shalat_hal_17.jpg	3/6/2022 6:06	3/6/2022 6:06
20	1	20	upload/books/pages/1/Shalat_hal_18.jpg	3/7/2022 6:06	3/7/2022 6:06
21	1	21	upload/books/pages/1/Shalat_hal_19.jpg	3/8/2022 6:06	3/8/2022 6:06
22	1	22	upload/books/pages/1/Shalat_hal_20.jpg	3/9/2022 6:06	3/9/2022 6:06
23	1	23	upload/books/pages/1/Shalat_hal_21.jpg	3/10/2022 6:06	3/10/2022 6:06
24	1	24	upload/books/pages/1/Shalat_hal_22.jpg	3/11/2022 6:06	3/11/2022 6:06
25	1	25	upload/books/pages/1/Shalat_hal_23.jpg	3/12/2022 6:06	3/12/2022 6:06
26	1	26	upload/books/pages/1/Shalat_hal_24.jpg	3/13/2022 6:06	3/13/2022 6:06
27	1	27	upload/books/pages/1/Shalat_hal_25.jpg	3/14/2022 6:06	3/14/2022 6:06
28	1	28	upload/books/pages/1/Shalat_hal_26.jpg	3/15/2022 6:06	3/15/2022 6:06
29	1	29	upload/books/pages/1/Shalat_hal_27.jpg	3/16/2022 6:06	3/16/2022 6:06
30	1	30	upload/books/pages/1/Shalat_hal_28.jpg	3/17/2022 6:06	3/17/2022 6:06
31	1	31	upload/books/pages/1/Shalat_hal_29.jpg	3/18/2022 6:06	3/18/2022 6:06
32	1	32	upload/books/pages/1/Shalat_hal_30.jpg	3/19/2022 6:06	3/19/2022 6:06
33	1	33	upload/books/pages/1/Shalat_hal_31.jpg	3/20/2022 6:06	3/20/2022 6:06
34	1	34	upload/books/pages/1/Shalat_hal_32.jpg	3/21/2022 6:06	3/21/2022 6:06
35	1	35	upload/books/pages/1/Shalat_hal_33.jpg	3/22/2022 6:06	3/22/2022 6:06
36	1	36	upload/books/pages/1/Shalat_hal_34.jpg	3/23/2022 6:06	3/23/2022 6:06
37	1	37	upload/books/pages/1/Shalat_hal_35.jpg	3/24/2022 6:06	3/24/2022 6:06
38	1	38	upload/books/pages/1/Shalat_hal_36.jpg	3/25/2022 6:06	3/25/2022 6:06
39	1	39	upload/books/pages/1/Shalat_hal_37.jpg	3/26/2022 6:06	3/26/2022 6:06
40	1	40	upload/books/pages/1/Shalat_hal_38.jpg	3/27/2022 6:06	3/27/2022 6:06
41	1	41	upload/books/pages/1/Shalat_hal_39.jpg	3/28/2022 6:06	3/28/2022 6:06
42	1	42	upload/books/pages/1/Shalat_hal_40.jpg	3/29/2022 6:06	3/29/2022 6:06
43	1	43	upload/books/pages/1/Shalat_hal_41.jpg	3/30/2022 6:06	3/30/2022 6:06
44	1	44	upload/books/pages/1/Shalat_hal_42.jpg	3/31/2022 6:06	3/31/2022 6:06
45	1	45	upload/books/pages/1/Shalat_hal_43.jpg	4/1/2022 6:06	4/1/2022 6:06
46	1	46	upload/books/pages/1/Shalat_hal_44.jpg	4/2/2022 6:06	4/2/2022 6:06
47	1	47	upload/books/pages/1/Shalat_hal_45.jpg	4/3/2022 6:06	4/3/2022 6:06
48	1	48	upload/books/pages/1/Shalat_hal_46.jpg	4/4/2022 6:06	4/4/2022 6:06
49	1	49	upload/books/pages/1/Shalat_hal_47.jpg	4/5/2022 6:06	4/5/2022 6:06
50	1	50	upload/books/pages/1/Shalat_hal_48.jpg	4/6/2022 6:06	4/6/2022 6:06
51	1	51	upload/books/pages/1/Shalat_hal_49.jpg	4/7/2022 6:06	4/7/2022 6:06
52	1	52	upload/books/pages/1/Shalat_hal_50.jpg	4/8/2022 6:06	4/8/2022 6:06
53	1	53	upload/books/pages/1/Shalat_hal_51.jpg	4/9/2022 6:06	4/9/2022 6:06
54	1	54	upload/books/pages/1/Shalat_hal_52.jpg	4/10/2022 6:06	4/10/2022 6:06
55	1	55	upload/books/pages/1/Shalat_hal_53.jpg	4/11/2022 6:06	4/11/2022 6:06
56	1	56	upload/books/pages/1/Shalat_hal_54.jpg	4/12/2022 6:06	4/12/2022 6:06
57	1	57	upload/books/pages/1/Shalat_hal_55.jpg	4/13/2022 6:06	4/13/2022 6:06
58	1	58	upload/books/pages/1/Shalat_hal_56.jpg	4/14/2022 6:06	4/14/2022 6:06
59	1	59	upload/books/pages/1/Shalat_hal_57.jpg	4/15/2022 6:06	4/15/2022 6:06
60	1	60	upload/books/pages/1/Shalat_hal_58.jpg	4/16/2022 6:06	4/16/2022 6:06
61	1	61	upload/books/pages/1/Shalat_hal_59.jpg	4/17/2022 6:06	4/17/2022 6:06
62	1	62	upload/books/pages/1/Shalat_hal_60.jpg	4/18/2022 6:06	4/18/2022 6:06
63	1	63	upload/books/pages/1/Shalat_hal_61.jpg	4/19/2022 6:06	4/19/2022 6:06
64	1	64	upload/books/pages/1/Shalat_hal_62.jpg	4/20/2022 6:06	4/20/2022 6:06
65	1	65	upload/books/pages/1/Shalat_hal_63.jpg	4/21/2022 6:06	4/21/2022 6:06
66	1	66	upload/books/pages/1/Shalat_hal_64.jpg	4/22/2022 6:06	4/22/2022 6:06
67	1	67	upload/books/pages/1/Shalat_hal_65.jpg	4/23/2022 6:06	4/23/2022 6:06
68	1	68	upload/books/pages/1/Shalat_hal_66.jpg	4/24/2022 6:06	4/24/2022 6:06
69	1	69	upload/books/pages/1/Shalat_hal_67.jpg	4/25/2022 6:06	4/25/2022 6:06
70	1	70	upload/books/pages/1/Shalat_hal_68.jpg	4/26/2022 6:06	4/26/2022 6:06
71	1	71	upload/books/pages/1/Shalat_hal_69.jpg	4/27/2022 6:06	4/27/2022 6:06
72	1	72	upload/books/pages/1/Shalat_hal_70.jpg	4/28/2022 6:06	4/28/2022 6:06
73	1	73	upload/books/pages/1/Shalat_hal_71.jpg	4/29/2022 6:06	4/29/2022 6:06
74	1	74	upload/books/pages/1/Shalat_hal_72.jpg	4/30/2022 6:06	4/30/2022 6:06
75	1	75	upload/books/pages/1/Shalat_hal_73.jpg	5/1/2022 6:06	5/1/2022 6:06
76	1	76	upload/books/pages/1/Shalat_hal_74.jpg	5/2/2022 6:06	5/2/2022 6:06
77	1	77	upload/books/pages/1/Shalat_hal_75.jpg	5/3/2022 6:06	5/3/2022 6:06
78	1	78	upload/books/pages/1/Shalat_hal_76.jpg	5/4/2022 6:06	5/4/2022 6:06
79	1	79	upload/books/pages/1/Shalat_hal_77.jpg	5/5/2022 6:06	5/5/2022 6:06
80	1	80	upload/books/pages/1/Shalat_hal_78.jpg	5/6/2022 6:06	5/6/2022 6:06
81	1	81	upload/books/pages/1/Shalat_hal_79.jpg	5/7/2022 6:06	5/7/2022 6:06
82	1	82	upload/books/pages/1/Shalat_hal_80.jpg	5/8/2022 6:06	5/8/2022 6:06
83	1	83	upload/books/pages/1/Shalat_hal_81.jpg	5/9/2022 6:06	5/9/2022 6:06
84	1	84	upload/books/pages/1/Shalat_hal_82.jpg	5/10/2022 6:06	5/10/2022 6:06
85	1	85	upload/books/pages/1/Shalat_hal_83.jpg	5/11/2022 6:06	5/11/2022 6:06
86	1	86	upload/books/pages/1/Shalat_hal_84.jpg	5/12/2022 6:06	5/12/2022 6:06
87	1	87	upload/books/pages/1/Shalat_hal_85.jpg	5/13/2022 6:06	5/13/2022 6:06
88	1	88	upload/books/pages/1/Shalat_hal_86.jpg	5/14/2022 6:06	5/14/2022 6:06
89	1	89	upload/books/pages/1/Shalat_hal_87.jpg	5/15/2022 6:06	5/15/2022 6:06
90	1	90	upload/books/pages/1/Shalat_hal_88.jpg	5/16/2022 6:06	5/16/2022 6:06
91	1	91	upload/books/pages/1/Shalat_hal_89.jpg	5/17/2022 6:06	5/17/2022 6:06
92	1	92	upload/books/pages/1/Shalat_hal_90.jpg	5/18/2022 6:06	5/18/2022 6:06
93	1	93	upload/books/pages/1/Shalat_hal_91.jpg	5/19/2022 6:06	5/19/2022 6:06
94	1	94	upload/books/pages/1/Shalat_hal_92.jpg	5/20/2022 6:06	5/20/2022 6:06
95	1	95	upload/books/pages/1/Shalat_hal_93.jpg	5/21/2022 6:06	5/21/2022 6:06
96	1	96	upload/books/pages/1/Shalat_hal_94.jpg	5/22/2022 6:06	5/22/2022 6:06
97	1	97	upload/books/pages/1/Shalat_hal_95.jpg	5/23/2022 6:06	5/23/2022 6:06
98	1	98	upload/books/pages/1/Shalat_hal_96.jpg	5/24/2022 6:06	5/24/2022 6:06
99	1	99	upload/books/pages/1/Shalat_hal_97.jpg	5/25/2022 6:06	5/25/2022 6:06
100	1	100	upload/books/pages/1/Shalat_hal_98.jpg	5/26/2022 6:06	5/26/2022 6:06
101	1	101	upload/books/pages/1/Shalat_hal_99.jpg	5/27/2022 6:06	5/27/2022 6:06
102	1	102	upload/books/pages/1/Shalat_hal_100.jpg	5/28/2022 6:06	5/28/2022 6:06
103	1	103	upload/books/pages/1/Shalat_hal_101.jpg	5/29/2022 6:06	5/29/2022 6:06
104	1	104	upload/books/pages/1/Shalat_hal_102.jpg	5/30/2022 6:06	5/30/2022 6:06
105	1	105	upload/books/pages/1/Shalat_hal_103.jpg	5/31/2022 6:06	5/31/2022 6:06
106	1	106	upload/books/pages/1/Shalat_hal_104.jpg	6/1/2022 6:06	6/1/2022 6:06
107	1	107	upload/books/pages/1/Shalat_hal_105.jpg	6/2/2022 6:06	6/2/2022 6:06
108	1	108	upload/books/pages/1/Shalat_hal_106.jpg	6/3/2022 6:06	6/3/2022 6:06
109	1	109	upload/books/pages/1/Shalat_hal_107.jpg	6/4/2022 6:06	6/4/2022 6:06
110	1	110	upload/books/pages/1/Shalat_hal_108.jpg	6/5/2022 6:06	6/5/2022 6:06
111	2	1	upload/books/pages/2/Puasa_halb.jpg	6/6/2022 6:06	6/6/2022 6:06
112	2	2	upload/books/pages/2/Puasa_halc.jpg	6/7/2022 6:06	6/7/2022 6:06
113	2	3	upload/books/pages/2/Puasa_hal1.jpg	6/8/2022 6:06	6/8/2022 6:06
114	2	4	upload/books/pages/2/Puasa_hal2.jpg	6/9/2022 6:06	6/9/2022 6:06
115	2	5	upload/books/pages/2/Puasa_hal3.jpg	6/10/2022 6:06	6/10/2022 6:06
116	2	6	upload/books/pages/2/Puasa_hal4.jpg	6/11/2022 6:06	6/11/2022 6:06
117	2	7	upload/books/pages/2/Puasa_hal5.jpg	6/12/2022 6:06	6/12/2022 6:06
118	2	8	upload/books/pages/2/Puasa_hal6.jpg	6/13/2022 6:06	6/13/2022 6:06
119	2	9	upload/books/pages/2/Puasa_hal7.jpg	6/14/2022 6:06	6/14/2022 6:06
120	2	10	upload/books/pages/2/Puasa_hal8.jpg	6/15/2022 6:06	6/15/2022 6:06
121	2	11	upload/books/pages/2/Puasa_hal9.jpg	6/16/2022 6:06	6/16/2022 6:06
122	2	12	upload/books/pages/2/Puasa_hal10.jpg	6/17/2022 6:06	6/17/2022 6:06
123	2	13	upload/books/pages/2/Puasa_hal11.jpg	6/18/2022 6:06	6/18/2022 6:06
124	2	14	upload/books/pages/2/Puasa_hal12.jpg	6/19/2022 6:06	6/19/2022 6:06
125	2	15	upload/books/pages/2/Puasa_hal13.jpg	6/20/2022 6:06	6/20/2022 6:06
126	2	16	upload/books/pages/2/Puasa_hal14.jpg	6/21/2022 6:06	6/21/2022 6:06
127	2	17	upload/books/pages/2/Puasa_hal15.jpg	6/22/2022 6:06	6/22/2022 6:06
128	2	18	upload/books/pages/2/Puasa_hal16.jpg	6/23/2022 6:06	6/23/2022 6:06
129	2	19	upload/books/pages/2/Puasa_hal17.jpg	6/24/2022 6:06	6/24/2022 6:06
130	2	20	upload/books/pages/2/Puasa_hal18.jpg	6/25/2022 6:06	6/25/2022 6:06
131	2	21	upload/books/pages/2/Puasa_hal19.jpg	6/26/2022 6:06	6/26/2022 6:06
132	2	22	upload/books/pages/2/Puasa_hal20.jpg	6/27/2022 6:06	6/27/2022 6:06
133	2	23	upload/books/pages/2/Puasa_hal21.jpg	6/28/2022 6:06	6/28/2022 6:06
134	2	24	upload/books/pages/2/Puasa_hal22.jpg	6/29/2022 6:06	6/29/2022 6:06
135	2	25	upload/books/pages/2/Puasa_hal23.jpg	6/30/2022 6:06	6/30/2022 6:06
136	2	26	upload/books/pages/2/Puasa_hal24.jpg	7/1/2022 6:06	7/1/2022 6:06
137	2	27	upload/books/pages/2/Puasa_hal25.jpg	7/2/2022 6:06	7/2/2022 6:06
138	2	28	upload/books/pages/2/Puasa_hal26.jpg	7/3/2022 6:06	7/3/2022 6:06
139	2	29	upload/books/pages/2/Puasa_hal27.jpg	7/4/2022 6:06	7/4/2022 6:06
140	2	30	upload/books/pages/2/Puasa_hal28.jpg	7/5/2022 6:06	7/5/2022 6:06
141	2	31	upload/books/pages/2/Puasa_hal29.jpg	7/6/2022 6:06	7/6/2022 6:06
142	2	32	upload/books/pages/2/Puasa_hal30.jpg	7/7/2022 6:06	7/7/2022 6:06
143	2	33	upload/books/pages/2/Puasa_hal31.jpg	7/8/2022 6:06	7/8/2022 6:06
144	2	34	upload/books/pages/2/Puasa_hal32.jpg	7/9/2022 6:06	7/9/2022 6:06
145	2	35	upload/books/pages/2/Puasa_hal33.jpg	7/10/2022 6:06	7/10/2022 6:06
146	2	36	upload/books/pages/2/Puasa_hal34.jpg	7/11/2022 6:06	7/11/2022 6:06
147	2	37	upload/books/pages/2/Puasa_hal35.jpg	7/12/2022 6:06	7/12/2022 6:06
148	2	38	upload/books/pages/2/Puasa_hal36.jpg	7/13/2022 6:06	7/13/2022 6:06
149	2	39	upload/books/pages/2/Puasa_hal37.jpg	7/14/2022 6:06	7/14/2022 6:06
150	2	40	upload/books/pages/2/Puasa_hal38.jpg	7/15/2022 6:06	7/15/2022 6:06
151	2	41	upload/books/pages/2/Puasa_hal39.jpg	7/16/2022 6:06	7/16/2022 6:06
152	2	42	upload/books/pages/2/Puasa_hal40.jpg	7/17/2022 6:06	7/17/2022 6:06
153	2	43	upload/books/pages/2/Puasa_hal41.jpg	7/18/2022 6:06	7/18/2022 6:06
154	2	44	upload/books/pages/2/Puasa_hal42.jpg	7/19/2022 6:06	7/19/2022 6:06
155	2	45	upload/books/pages/2/Puasa_hal43.jpg	7/20/2022 6:06	7/20/2022 6:06
156	2	46	upload/books/pages/2/Puasa_hal44.jpg	7/21/2022 6:06	7/21/2022 6:06
157	2	47	upload/books/pages/2/Puasa_hal45.jpg	7/22/2022 6:06	7/22/2022 6:06
158	2	48	upload/books/pages/2/Puasa_hal46.jpg	7/23/2022 6:06	7/23/2022 6:06
159	2	49	upload/books/pages/2/Puasa_hal47.jpg	7/24/2022 6:06	7/24/2022 6:06
160	2	50	upload/books/pages/2/Puasa_hal48.jpg	7/25/2022 6:06	7/25/2022 6:06
161	2	51	upload/books/pages/2/Puasa_hal49.jpg	7/26/2022 6:06	7/26/2022 6:06
162	2	52	upload/books/pages/2/Puasa_hal50.jpg	7/27/2022 6:06	7/27/2022 6:06
163	2	53	upload/books/pages/2/Puasa_hal51.jpg	7/28/2022 6:06	7/28/2022 6:06
164	2	54	upload/books/pages/2/Puasa_hal52.jpg	7/29/2022 6:06	7/29/2022 6:06
165	2	55	upload/books/pages/2/Puasa_hal53.jpg	7/30/2022 6:06	7/30/2022 6:06
166	2	56	upload/books/pages/2/Puasa_hal54.jpg	7/31/2022 6:06	7/31/2022 6:06
167	2	57	upload/books/pages/2/Puasa_hal55.jpg	8/1/2022 6:06	8/1/2022 6:06
168	2	58	upload/books/pages/2/Puasa_hal56.jpg	8/2/2022 6:06	8/2/2022 6:06
169	2	59	upload/books/pages/2/Puasa_hal57.jpg	8/3/2022 6:06	8/3/2022 6:06
170	2	60	upload/books/pages/2/Puasa_hal58.jpg	8/4/2022 6:06	8/4/2022 6:06
171	3	1	upload/books/pages/2/Zakat_hala.jpg	8/5/2022 6:06	8/5/2022 6:06
172	3	2	upload/books/pages/2/Zakat_halb.jpg	8/6/2022 6:06	8/6/2022 6:06
173	3	3	upload/books/pages/2/Zakat_hal1.jpg	8/7/2022 6:06	8/7/2022 6:06
174	3	4	upload/books/pages/2/Zakat_hal2.jpg	8/8/2022 6:06	8/8/2022 6:06
175	3	5	upload/books/pages/2/Zakat_hal3.jpg	8/9/2022 6:06	8/9/2022 6:06
176	3	6	upload/books/pages/2/Zakat_hal4.jpg	8/10/2022 6:06	8/10/2022 6:06
177	3	7	upload/books/pages/2/Zakat_hal5.jpg	8/11/2022 6:06	8/11/2022 6:06
178	3	8	upload/books/pages/2/Zakat_hal6.jpg	8/12/2022 6:06	8/12/2022 6:06
179	3	9	upload/books/pages/2/Zakat_hal7.jpg	8/13/2022 6:06	8/13/2022 6:06
180	3	10	upload/books/pages/2/Zakat_hal8.jpg	8/14/2022 6:06	8/14/2022 6:06
181	3	11	upload/books/pages/2/Zakat_hal9.jpg	8/15/2022 6:06	8/15/2022 6:06
182	3	12	upload/books/pages/2/Zakat_hal10.jpg	8/16/2022 6:06	8/16/2022 6:06
183	3	13	upload/books/pages/2/Zakat_hal11.jpg	8/17/2022 6:06	8/17/2022 6:06
184	3	14	upload/books/pages/2/Zakat_hal12.jpg	8/18/2022 6:06	8/18/2022 6:06
185	3	15	upload/books/pages/2/Zakat_hal13.jpg	8/19/2022 6:06	8/19/2022 6:06
186	3	16	upload/books/pages/2/Zakat_hal14.jpg	8/20/2022 6:06	8/20/2022 6:06
187	3	17	upload/books/pages/2/Zakat_hal15.jpg	8/21/2022 6:06	8/21/2022 6:06
188	3	18	upload/books/pages/2/Zakat_hal16.jpg	8/22/2022 6:06	8/22/2022 6:06
189	3	19	upload/books/pages/2/Zakat_hal17.jpg	8/23/2022 6:06	8/23/2022 6:06
190	3	20	upload/books/pages/2/Zakat_hal18.jpg	8/24/2022 6:06	8/24/2022 6:06
191	3	21	upload/books/pages/2/Zakat_hal19.jpg	8/25/2022 6:06	8/25/2022 6:06
192	3	22	upload/books/pages/2/Zakat_hal20.jpg	8/26/2022 6:06	8/26/2022 6:06
193	3	23	upload/books/pages/2/Zakat_hal21.jpg	8/27/2022 6:06	8/27/2022 6:06
194	3	24	upload/books/pages/2/Zakat_hal22.jpg	8/28/2022 6:06	8/28/2022 6:06
195	3	25	upload/books/pages/2/Zakat_hal23.jpg	8/29/2022 6:06	8/29/2022 6:06
196	3	26	upload/books/pages/2/Zakat_hal24.jpg	8/30/2022 6:06	8/30/2022 6:06
197	3	27	upload/books/pages/2/Zakat_hal25.jpg	8/31/2022 6:06	8/31/2022 6:06
198	3	28	upload/books/pages/2/Zakat_hal26.jpg	9/1/2022 6:06	9/1/2022 6:06
199	3	29	upload/books/pages/2/Zakat_hal27.jpg	9/2/2022 6:06	9/2/2022 6:06
200	3	30	upload/books/pages/2/Zakat_hal28.jpg	9/3/2022 6:06	9/3/2022 6:06
201	3	31	upload/books/pages/2/Zakat_hal29.jpg	9/4/2022 6:06	9/4/2022 6:06
202	3	32	upload/books/pages/2/Zakat_hal30.jpg	9/5/2022 6:06	9/5/2022 6:06
203	3	33	upload/books/pages/2/Zakat_hal31.jpg	9/6/2022 6:06	9/6/2022 6:06
204	3	34	upload/books/pages/2/Zakat_hal32.jpg	9/7/2022 6:06	9/7/2022 6:06
205	3	35	upload/books/pages/2/Zakat_hal33.jpg	9/8/2022 6:06	9/8/2022 6:06
206	3	36	upload/books/pages/2/Zakat_hal34.jpg	9/9/2022 6:06	9/9/2022 6:06
207	3	37	upload/books/pages/2/Zakat_hal35.jpg	9/10/2022 6:06	9/10/2022 6:06
208	3	38	upload/books/pages/2/Zakat_hal36.jpg	9/11/2022 6:06	9/11/2022 6:06
209	3	39	upload/books/pages/2/Zakat_hal37.jpg	9/12/2022 6:06	9/12/2022 6:06
210	3	40	upload/books/pages/2/Zakat_hal38.jpg	9/13/2022 6:06	9/13/2022 6:06
211	3	41	upload/books/pages/2/Zakat_hal39.jpg	9/14/2022 6:06	9/14/2022 6:06
212	3	42	upload/books/pages/2/Zakat_hal40.jpg	9/15/2022 6:06	9/15/2022 6:06
213	3	43	upload/books/pages/2/Zakat_hal41.jpg	9/16/2022 6:06	9/16/2022 6:06
214	3	44	upload/books/pages/2/Zakat_hal42.jpg	9/17/2022 6:06	9/17/2022 6:06
215	3	45	upload/books/pages/2/Zakat_hal43.jpg	9/18/2022 6:06	9/18/2022 6:06
216	3	46	upload/books/pages/2/Zakat_hal44.jpg	9/19/2022 6:06	9/19/2022 6:06
217	3	47	upload/books/pages/2/Zakat_hal45.jpg	9/20/2022 6:06	9/20/2022 6:06
218	3	48	upload/books/pages/2/Zakat_hal46.jpg	9/21/2022 6:06	9/21/2022 6:06
219	3	49	upload/books/pages/2/Zakat_hal47.jpg	9/22/2022 6:06	9/22/2022 6:06
220	3	50	upload/books/pages/2/Zakat_hal48.jpg	9/23/2022 6:06	9/23/2022 6:06
221	3	51	upload/books/pages/2/Zakat_hal49.jpg	9/24/2022 6:06	9/24/2022 6:06
222	3	52	upload/books/pages/2/Zakat_hal50.jpg	9/25/2022 6:06	9/25/2022 6:06
223	3	53	upload/books/pages/2/Zakat_hal51.jpg	9/26/2022 6:06	9/26/2022 6:06
224	3	54	upload/books/pages/2/Zakat_hal52.jpg	9/27/2022 6:06	9/27/2022 6:06
225	3	55	upload/books/pages/2/Zakat_hal53.jpg	9/28/2022 6:06	9/28/2022 6:06
226	3	56	upload/books/pages/2/Zakat_hal54.jpg	9/29/2022 6:06	9/29/2022 6:06
227	3	57	upload/books/pages/2/Zakat_hal55.jpg	9/30/2022 6:06	9/30/2022 6:06
228	3	58	upload/books/pages/2/Zakat_hal56.jpg	10/1/2022 6:06	10/1/2022 6:06
229	3	59	upload/books/pages/2/Zakat_hal57.jpg	10/2/2022 6:06	10/2/2022 6:06
230	3	60	upload/books/pages/2/Zakat_hal58.jpg	10/3/2022 6:06	10/3/2022 6:06
231	4	1	upload/books/pages/2/HajiUmroh_hala.jpg	10/4/2022 6:06	10/4/2022 6:06
232	4	2	upload/books/pages/2/HajiUmroh_halb.jpg	10/5/2022 6:06	10/5/2022 6:06
233	4	3	upload/books/pages/2/HajiUmroh_hal1.jpg	10/6/2022 6:06	10/6/2022 6:06
234	4	4	upload/books/pages/2/HajiUmroh_hal2.jpg	10/7/2022 6:06	10/7/2022 6:06
235	4	5	upload/books/pages/2/HajiUmroh_hal3.jpg	10/8/2022 6:06	10/8/2022 6:06
236	4	6	upload/books/pages/2/HajiUmroh_hal4.jpg	10/9/2022 6:06	10/9/2022 6:06
237	4	7	upload/books/pages/2/HajiUmroh_hal5.jpg	10/10/2022 6:06	10/10/2022 6:06
238	4	8	upload/books/pages/2/HajiUmroh_hal6.jpg	10/11/2022 6:06	10/11/2022 6:06
239	4	9	upload/books/pages/2/HajiUmroh_hal7.jpg	10/12/2022 6:06	10/12/2022 6:06
240	4	10	upload/books/pages/2/HajiUmroh_hal8.jpg	10/13/2022 6:06	10/13/2022 6:06
241	4	11	upload/books/pages/2/HajiUmroh_hal9.jpg	10/14/2022 6:06	10/14/2022 6:06
242	4	12	upload/books/pages/2/HajiUmroh_hal10.jpg	10/15/2022 6:06	10/15/2022 6:06
243	4	13	upload/books/pages/2/HajiUmroh_hal11.jpg	10/16/2022 6:06	10/16/2022 6:06
244	4	14	upload/books/pages/2/HajiUmroh_hal12.jpg	10/17/2022 6:06	10/17/2022 6:06
245	4	15	upload/books/pages/2/HajiUmroh_hal13.jpg	10/18/2022 6:06	10/18/2022 6:06
246	4	16	upload/books/pages/2/HajiUmroh_hal14.jpg	10/19/2022 6:06	10/19/2022 6:06
247	4	17	upload/books/pages/2/HajiUmroh_hal15.jpg	10/20/2022 6:06	10/20/2022 6:06
248	4	18	upload/books/pages/2/HajiUmroh_hal16.jpg	10/21/2022 6:06	10/21/2022 6:06
249	4	19	upload/books/pages/2/HajiUmroh_hal17.jpg	10/22/2022 6:06	10/22/2022 6:06
250	4	20	upload/books/pages/2/HajiUmroh_hal18.jpg	10/23/2022 6:06	10/23/2022 6:06
251	4	21	upload/books/pages/2/HajiUmroh_hal19.jpg	10/24/2022 6:06	10/24/2022 6:06
252	4	22	upload/books/pages/2/HajiUmroh_hal20.jpg	10/25/2022 6:06	10/25/2022 6:06
253	4	23	upload/books/pages/2/HajiUmroh_hal21.jpg	10/26/2022 6:06	10/26/2022 6:06
254	4	24	upload/books/pages/2/HajiUmroh_hal22.jpg	10/27/2022 6:06	10/27/2022 6:06
255	4	25	upload/books/pages/2/HajiUmroh_hal23.jpg	10/28/2022 6:06	10/28/2022 6:06
256	4	26	upload/books/pages/2/HajiUmroh_hal24.jpg	10/29/2022 6:06	10/29/2022 6:06
257	4	27	upload/books/pages/2/HajiUmroh_hal25.jpg	10/30/2022 6:06	10/30/2022 6:06
258	4	28	upload/books/pages/2/HajiUmroh_hal26.jpg	10/31/2022 6:06	10/31/2022 6:06
259	4	29	upload/books/pages/2/HajiUmroh_hal27.jpg	11/1/2022 6:06	11/1/2022 6:06
260	4	30	upload/books/pages/2/HajiUmroh_hal28.jpg	11/2/2022 6:06	11/2/2022 6:06
261	4	31	upload/books/pages/2/HajiUmroh_hal29.jpg	11/3/2022 6:06	11/3/2022 6:06
262	4	32	upload/books/pages/2/HajiUmroh_hal30.jpg	11/4/2022 6:06	11/4/2022 6:06
263	4	33	upload/books/pages/2/HajiUmroh_hal31.jpg	11/5/2022 6:06	11/5/2022 6:06
264	4	34	upload/books/pages/2/HajiUmroh_hal32.jpg	11/6/2022 6:06	11/6/2022 6:06
265	4	35	upload/books/pages/2/HajiUmroh_hal33.jpg	11/7/2022 6:06	11/7/2022 6:06
266	4	36	upload/books/pages/2/HajiUmroh_hal34.jpg	11/8/2022 6:06	11/8/2022 6:06
267	4	37	upload/books/pages/2/HajiUmroh_hal35.jpg	11/9/2022 6:06	11/9/2022 6:06
268	4	38	upload/books/pages/2/HajiUmroh_hal36.jpg	11/10/2022 6:06	11/10/2022 6:06
269	4	39	upload/books/pages/2/HajiUmroh_hal37.jpg	11/11/2022 6:06	11/11/2022 6:06
270	4	40	upload/books/pages/2/HajiUmroh_hal38.jpg	11/12/2022 6:06	11/12/2022 6:06
271	4	41	upload/books/pages/2/HajiUmroh_hal39.jpg	11/13/2022 6:06	11/13/2022 6:06
272	4	42	upload/books/pages/2/HajiUmroh_hal40.jpg	11/14/2022 6:06	11/14/2022 6:06
273	4	43	upload/books/pages/2/HajiUmroh_hal41.jpg	11/15/2022 6:06	11/15/2022 6:06
274	4	44	upload/books/pages/2/HajiUmroh_hal42.jpg	11/16/2022 6:06	11/16/2022 6:06
275	4	45	upload/books/pages/2/HajiUmroh_hal43.jpg	11/17/2022 6:06	11/17/2022 6:06
276	4	46	upload/books/pages/2/HajiUmroh_hal44.jpg	11/18/2022 6:06	11/18/2022 6:06
277	4	47	upload/books/pages/2/HajiUmroh_hal45.jpg	11/19/2022 6:06	11/19/2022 6:06
278	4	48	upload/books/pages/2/HajiUmroh_hal46.jpg	11/20/2022 6:06	11/20/2022 6:06
279	4	49	upload/books/pages/2/HajiUmroh_hal47.jpg	11/21/2022 6:06	11/21/2022 6:06
280	4	50	upload/books/pages/2/HajiUmroh_hal48.jpg	11/22/2022 6:06	11/22/2022 6:06
281	4	51	upload/books/pages/2/HajiUmroh_hal49.jpg	11/23/2022 6:06	11/23/2022 6:06
282	4	52	upload/books/pages/2/HajiUmroh_hal50.jpg	11/24/2022 6:06	11/24/2022 6:06
283	4	53	upload/books/pages/2/HajiUmroh_hal51.jpg	11/25/2022 6:06	11/25/2022 6:06
284	4	54	upload/books/pages/2/HajiUmroh_hal52.jpg	11/26/2022 6:06	11/26/2022 6:06
285	4	55	upload/books/pages/2/HajiUmroh_hal53.jpg	11/27/2022 6:06	11/27/2022 6:06
286	4	56	upload/books/pages/2/HajiUmroh_hal54.jpg	11/28/2022 6:06	11/28/2022 6:06
287	4	57	upload/books/pages/2/HajiUmroh_hal55.jpg	11/29/2022 6:06	11/29/2022 6:06
288	4	58	upload/books/pages/2/HajiUmroh_hal56.jpg	11/30/2022 6:06	11/30/2022 6:06
289	4	59	upload/books/pages/2/HajiUmroh_hal57.jpg	12/1/2022 6:06	12/1/2022 6:06
290	4	60	upload/books/pages/2/HajiUmroh_hal58.jpg	12/2/2022 6:06	12/2/2022 6:06
291	4	61	upload/books/pages/2/HajiUmroh_hal59.jpg	12/3/2022 6:06	12/3/2022 6:06
292	4	62	upload/books/pages/2/HajiUmroh_hal60.jpg	12/4/2022 6:06	12/4/2022 6:06
293	4	63	upload/books/pages/2/HajiUmroh_hal61.jpg	12/5/2022 6:06	12/5/2022 6:06
294	4	64	upload/books/pages/2/HajiUmroh_hal62.jpg	12/6/2022 6:06	12/6/2022 6:06
295	4	65	upload/books/pages/2/HajiUmroh_hal63.jpg	12/7/2022 6:06	12/7/2022 6:06
296	4	66	upload/books/pages/2/HajiUmroh_hal64.jpg	12/8/2022 6:06	12/8/2022 6:06
297	4	67	upload/books/pages/2/HajiUmroh_hal65.jpg	12/9/2022 6:06	12/9/2022 6:06
298	4	68	upload/books/pages/2/HajiUmroh_hal66.jpg	12/10/2022 6:06	12/10/2022 6:06
299	4	69	upload/books/pages/2/HajiUmroh_hal67.jpg	12/11/2022 6:06	12/11/2022 6:06
300	4	70	upload/books/pages/2/HajiUmroh_hal68.jpg	12/12/2022 6:06	12/12/2022 6:06
301	4	71	upload/books/pages/2/HajiUmroh_hal69.jpg	12/13/2022 6:06	12/13/2022 6:06
302	4	72	upload/books/pages/2/HajiUmroh_hal70.jpg	12/14/2022 6:06	12/14/2022 6:06
303	4	73	upload/books/pages/2/HajiUmroh_hal71.jpg	12/15/2022 6:06	12/15/2022 6:06
304	4	74	upload/books/pages/2/HajiUmroh_hal72.jpg	12/16/2022 6:06	12/16/2022 6:06
305	4	75	upload/books/pages/2/HajiUmroh_hal73.jpg	12/17/2022 6:06	12/17/2022 6:06
306	4	76	upload/books/pages/2/HajiUmroh_hal74.jpg	12/18/2022 6:06	12/18/2022 6:06
307	4	77	upload/books/pages/2/HajiUmroh_hal75.jpg	12/19/2022 6:06	12/19/2022 6:06
308	4	78	upload/books/pages/2/HajiUmroh_hal76.jpg	12/20/2022 6:06	12/20/2022 6:06
309	4	79	upload/books/pages/2/HajiUmroh_hal77.jpg	12/21/2022 6:06	12/21/2022 6:06
310	4	80	upload/books/pages/2/HajiUmroh_hal78.jpg	12/22/2022 6:06	12/22/2022 6:06
311	4	81	upload/books/pages/2/HajiUmroh_hal79.jpg	12/23/2022 6:06	12/23/2022 6:06
312	4	82	upload/books/pages/2/HajiUmroh_hal80.jpg	12/24/2022 6:06	12/24/2022 6:06
313	4	83	upload/books/pages/2/HajiUmroh_hal81.jpg	12/25/2022 6:06	12/25/2022 6:06
314	4	84	upload/books/pages/2/HajiUmroh_hal82.jpg	12/26/2022 6:06	12/26/2022 6:06
315	4	85	upload/books/pages/2/HajiUmroh_hal83.jpg	12/27/2022 6:06	12/27/2022 6:06
316	4	86	upload/books/pages/2/HajiUmroh_hal84.jpg	12/28/2022 6:06	12/28/2022 6:06
317	4	87	upload/books/pages/2/HajiUmroh_hal85.jpg	12/29/2022 6:06	12/29/2022 6:06
318	4	88	upload/books/pages/2/HajiUmroh_hal86.jpg	12/30/2022 6:06	12/30/2022 6:06
319	4	89	upload/books/pages/2/HajiUmroh_hal87.jpg	12/31/2022 6:06	12/31/2022 6:06
320	4	90	upload/books/pages/2/HajiUmroh_hal88.jpg	1/1/2023 6:06	1/1/2023 6:06
321	4	91	upload/books/pages/2/HajiUmroh_hal89.jpg	1/2/2023 6:06	1/2/2023 6:06
322	4	92	upload/books/pages/2/HajiUmroh_hal90.jpg	1/3/2023 6:06	1/3/2023 6:06
323	4	93	upload/books/pages/2/HajiUmroh_hal91.jpg	1/4/2023 6:06	1/4/2023 6:06
324	4	94	upload/books/pages/2/HajiUmroh_hal92.jpg	1/5/2023 6:06	1/5/2023 6:06
325	4	95	upload/books/pages/2/HajiUmroh_hal93.jpg	1/6/2023 6:06	1/6/2023 6:06
326	4	96	upload/books/pages/2/HajiUmroh_hal94.jpg	1/7/2023 6:06	1/7/2023 6:06
327	5	1	upload/books/pages/2/Mengaji_hala.jpg	1/8/2023 6:06	1/8/2023 6:06
328	5	2	upload/books/pages/2/Mengaji_halb.jpg	1/9/2023 6:06	1/9/2023 6:06
329	5	3	upload/books/pages/2/Mengaji_hal1.jpg	1/10/2023 6:06	1/10/2023 6:06
330	5	4	upload/books/pages/2/Mengaji_hal2.jpg	1/11/2023 6:06	1/11/2023 6:06
331	5	5	upload/books/pages/2/Mengaji_hal3.jpg	1/12/2023 6:06	1/12/2023 6:06
332	5	6	upload/books/pages/2/Mengaji_hal4.jpg	1/13/2023 6:06	1/13/2023 6:06
333	5	7	upload/books/pages/2/Mengaji_hal5.jpg	1/14/2023 6:06	1/14/2023 6:06
334	5	8	upload/books/pages/2/Mengaji_hal6.jpg	1/15/2023 6:06	1/15/2023 6:06
335	5	9	upload/books/pages/2/Mengaji_hal7.jpg	1/16/2023 6:06	1/16/2023 6:06
336	5	10	upload/books/pages/2/Mengaji_hal8.jpg	1/17/2023 6:06	1/17/2023 6:06
337	5	11	upload/books/pages/2/Mengaji_hal9.jpg	1/18/2023 6:06	1/18/2023 6:06
338	5	12	upload/books/pages/2/Mengaji_hal10.jpg	1/19/2023 6:06	1/19/2023 6:06
339	5	13	upload/books/pages/2/Mengaji_hal11.jpg	1/20/2023 6:06	1/20/2023 6:06
340	5	14	upload/books/pages/2/Mengaji_hal12.jpg	1/21/2023 6:06	1/21/2023 6:06
341	5	15	upload/books/pages/2/Mengaji_hal13.jpg	1/22/2023 6:06	1/22/2023 6:06
342	5	16	upload/books/pages/2/Mengaji_hal14.jpg	1/23/2023 6:06	1/23/2023 6:06
343	5	17	upload/books/pages/2/Mengaji_hal15.jpg	1/24/2023 6:06	1/24/2023 6:06
344	5	18	upload/books/pages/2/Mengaji_hal16.jpg	1/25/2023 6:06	1/25/2023 6:06
345	5	19	upload/books/pages/2/Mengaji_hal17.jpg	1/26/2023 6:06	1/26/2023 6:06
346	5	20	upload/books/pages/2/Mengaji_hal18.jpg	1/27/2023 6:06	1/27/2023 6:06
347	5	21	upload/books/pages/2/Mengaji_hal19.jpg	1/28/2023 6:06	1/28/2023 6:06
348	5	22	upload/books/pages/2/Mengaji_hal20.jpg	1/29/2023 6:06	1/29/2023 6:06
349	5	23	upload/books/pages/2/Mengaji_hal21.jpg	1/30/2023 6:06	1/30/2023 6:06
350	5	24	upload/books/pages/2/Mengaji_hal22.jpg	1/31/2023 6:06	1/31/2023 6:06
351	5	25	upload/books/pages/2/Mengaji_hal23.jpg	2/1/2023 6:06	2/1/2023 6:06
352	5	26	upload/books/pages/2/Mengaji_hal24.jpg	2/2/2023 6:06	2/2/2023 6:06
353	5	27	upload/books/pages/2/Mengaji_hal25.jpg	2/3/2023 6:06	2/3/2023 6:06
354	5	28	upload/books/pages/2/Mengaji_hal26.jpg	2/4/2023 6:06	2/4/2023 6:06
355	5	29	upload/books/pages/2/Mengaji_hal27.jpg	2/5/2023 6:06	2/5/2023 6:06
356	5	30	upload/books/pages/2/Mengaji_hal28.jpg	2/6/2023 6:06	2/6/2023 6:06
357	5	31	upload/books/pages/2/Mengaji_hal29.jpg	2/7/2023 6:06	2/7/2023 6:06
358	5	32	upload/books/pages/2/Mengaji_hal30.jpg	2/8/2023 6:06	2/8/2023 6:06
359	5	33	upload/books/pages/2/Mengaji_hal31.jpg	2/9/2023 6:06	2/9/2023 6:06
360	5	34	upload/books/pages/2/Mengaji_hal32.jpg	2/10/2023 6:06	2/10/2023 6:06
361	5	35	upload/books/pages/2/Mengaji_hal33.jpg	2/11/2023 6:06	2/11/2023 6:06
362	5	36	upload/books/pages/2/Mengaji_hal34.jpg	2/12/2023 6:06	2/12/2023 6:06
363	5	37	upload/books/pages/2/Mengaji_hal35.jpg	2/13/2023 6:06	2/13/2023 6:06
364	5	38	upload/books/pages/2/Mengaji_hal36.jpg	2/14/2023 6:06	2/14/2023 6:06
365	5	39	upload/books/pages/2/Mengaji_hal37.jpg	2/15/2023 6:06	2/15/2023 6:06
366	5	40	upload/books/pages/2/Mengaji_hal38.jpg	2/16/2023 6:06	2/16/2023 6:06
367	5	41	upload/books/pages/2/Mengaji_hal39.jpg	2/17/2023 6:06	2/17/2023 6:06
368	5	42	upload/books/pages/2/Mengaji_hal40.jpg	2/18/2023 6:06	2/18/2023 6:06
369	5	43	upload/books/pages/2/Mengaji_hal41.jpg	2/19/2023 6:06	2/19/2023 6:06
370	5	44	upload/books/pages/2/Mengaji_hal42.jpg	2/20/2023 6:06	2/20/2023 6:06
371	5	45	upload/books/pages/2/Mengaji_hal43.jpg	2/21/2023 6:06	2/21/2023 6:06
372	5	46	upload/books/pages/2/Mengaji_hal44.jpg	2/22/2023 6:06	2/22/2023 6:06
373	5	47	upload/books/pages/2/Mengaji_hal45.jpg	2/23/2023 6:06	2/23/2023 6:06
374	5	48	upload/books/pages/2/Mengaji_hal46.jpg	2/24/2023 6:06	2/24/2023 6:06
375	5	49	upload/books/pages/2/Mengaji_hal47.jpg	2/25/2023 6:06	2/25/2023 6:06
376	5	50	upload/books/pages/2/Mengaji_hal48.jpg	2/26/2023 6:06	2/26/2023 6:06
377	5	51	upload/books/pages/2/Mengaji_hal49.jpg	2/27/2023 6:06	2/27/2023 6:06
378	5	52	upload/books/pages/2/Mengaji_hal50.jpg	2/28/2023 6:06	2/28/2023 6:06
379	5	53	upload/books/pages/2/Mengaji_hal51.jpg	3/1/2023 6:06	3/1/2023 6:06
380	5	54	upload/books/pages/2/Mengaji_hal52.jpg	3/2/2023 6:06	3/2/2023 6:06
381	5	55	upload/books/pages/2/Mengaji_hal53.jpg	3/3/2023 6:06	3/3/2023 6:06
382	5	56	upload/books/pages/2/Mengaji_hal54.jpg	3/4/2023 6:06	3/4/2023 6:06
383	5	57	upload/books/pages/2/Mengaji_hal55.jpg	3/5/2023 6:06	3/5/2023 6:06
384	5	58	upload/books/pages/2/Mengaji_hal56.jpg	3/6/2023 6:06	3/6/2023 6:06
385	5	59	upload/books/pages/2/Mengaji_hal57.jpg	3/7/2023 6:06	3/7/2023 6:06
386	5	60	upload/books/pages/2/Mengaji_hal58.jpg	3/8/2023 6:06	3/8/2023 6:06
387	5	61	upload/books/pages/2/Mengaji_hal59.jpg	3/9/2023 6:06	3/9/2023 6:06
388	5	62	upload/books/pages/2/Mengaji_hal60.jpg	3/10/2023 6:06	3/10/2023 6:06
389	5	63	upload/books/pages/2/Mengaji_hal61.jpg	3/11/2023 6:06	3/11/2023 6:06
390	5	64	upload/books/pages/2/Mengaji_hal62.jpg	3/12/2023 6:06	3/12/2023 6:06
391	5	65	upload/books/pages/2/Mengaji_hal63.jpg	3/13/2023 6:06	3/13/2023 6:06
392	5	66	upload/books/pages/2/Mengaji_hal64.jpg	3/14/2023 6:06	3/14/2023 6:06
393	5	67	upload/books/pages/2/Mengaji_hal65.jpg	3/15/2023 6:06	3/15/2023 6:06
394	5	68	upload/books/pages/2/Mengaji_hal66.jpg	3/16/2023 6:06	3/16/2023 6:06
395	5	69	upload/books/pages/2/Mengaji_hal67.jpg	3/17/2023 6:06	3/17/2023 6:06
396	5	70	upload/books/pages/2/Mengaji_hal68.jpg	3/18/2023 6:06	3/18/2023 6:06
397	5	71	upload/books/pages/2/Mengaji_hal69.jpg	3/19/2023 6:06	3/19/2023 6:06
398	5	72	upload/books/pages/2/Mengaji_hal70.jpg	3/20/2023 6:06	3/20/2023 6:06
399	5	73	upload/books/pages/2/Mengaji_hal71.jpg	3/21/2023 6:06	3/21/2023 6:06
400	5	74	upload/books/pages/2/Mengaji_hal72.jpg	3/22/2023 6:06	3/22/2023 6:06
401	5	75	upload/books/pages/2/Mengaji_hal73.jpg	3/23/2023 6:06	3/23/2023 6:06
402	5	76	upload/books/pages/2/Mengaji_hal74.jpg	3/24/2023 6:06	3/24/2023 6:06
403	5	77	upload/books/pages/2/Mengaji_hal75.jpg	3/25/2023 6:06	3/25/2023 6:06
404	5	78	upload/books/pages/2/Mengaji_hal76.jpg	3/26/2023 6:06	3/26/2023 6:06
405	5	79	upload/books/pages/2/Mengaji_hal77.jpg	3/27/2023 6:06	3/27/2023 6:06
406	5	80	upload/books/pages/2/Mengaji_hal78.jpg	3/28/2023 6:06	3/28/2023 6:06
407	5	81	upload/books/pages/2/Mengaji_hal79.jpg	3/29/2023 6:06	3/29/2023 6:06
408	5	82	upload/books/pages/2/Mengaji_hal80.jpg	3/30/2023 6:06	3/30/2023 6:06
409	5	83	upload/books/pages/2/Mengaji_hal81.jpg	3/31/2023 6:06	3/31/2023 6:06
410	5	84	upload/books/pages/2/Mengaji_hal82.jpg	4/1/2023 6:06	4/1/2023 6:06
411	5	85	upload/books/pages/2/Mengaji_hal83.jpg	4/2/2023 6:06	4/2/2023 6:06
412	5	86	upload/books/pages/2/Mengaji_hal84.jpg	4/3/2023 6:06	4/3/2023 6:06
413	5	87	upload/books/pages/2/Mengaji_hal85.jpg	4/4/2023 6:06	4/4/2023 6:06
414	5	88	upload/books/pages/2/Mengaji_hal86.jpg	4/5/2023 6:06	4/5/2023 6:06
415	5	89	upload/books/pages/2/Mengaji_hal87.jpg	4/6/2023 6:06	4/6/2023 6:06
416	5	90	upload/books/pages/2/Mengaji_hal88.jpg	4/7/2023 6:06	4/7/2023 6:06
417	5	91	upload/books/pages/2/Mengaji_hal89.jpg	4/8/2023 6:06	4/8/2023 6:06
418	5	92	upload/books/pages/2/Mengaji_hal90.jpg	4/9/2023 6:06	4/9/2023 6:06
419	5	93	upload/books/pages/2/Mengaji_hal91.jpg	4/10/2023 6:06	4/10/2023 6:06
420	5	94	upload/books/pages/2/Mengaji_hal92.jpg	4/11/2023 6:06	4/11/2023 6:06
421	5	95	upload/books/pages/2/Mengaji_hal93.jpg	4/12/2023 6:06	4/12/2023 6:06
422	5	96	upload/books/pages/2/Mengaji_hal94.jpg	4/13/2023 6:06	4/13/2023 6:06
\.


--
-- Data for Name: _kids_tbl_sound; Type: TABLE DATA; Schema: public; Owner: plpgsql
--

COPY public.kids_tbl_sound (id, page_id, orders, sound_url, type, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: _master_role; Type: TABLE DATA; Schema: public; Owner: plpgsql
--

COPY public.master_role (id, level, name, created_at, updated_at) FROM stdin;
1	1	admin	2022-02-03 04:25:46	2022-02-03 04:25:46
\.


--
-- Data for Name: _tbl_user; Type: TABLE DATA; Schema: public; Owner: plpgsql
--

COPY public.tbl_user (id, name, role_id, gender, phone, username, password, created_at, updated_at) FROM stdin;
1	Admin	1	1		admin	$2a$12$3azbgvPZvNnj3s3X.8A/6eaQYk6aEwFQKlW1wfhaKDiTjdlLmPp/i	2022-02-03 04:26:24	2022-02-03 04:26:24
\.


--
-- PostgreSQL database dump complete
--

