-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2022 at 12:10 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cordoba_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `kids_master_book`
--

CREATE TABLE `kids_master_book` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `cover_url` varchar(256) DEFAULT NULL,
  `cover_sound_url` varchar(256) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `code` varchar(16) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kids_master_book`
--

INSERT INTO `kids_master_book` (`id`, `name`, `cover_url`, `cover_sound_url`, `number`, `code`, `created_at`, `updated_at`) VALUES
(12, 'Ajarkan Aku Cinta Sholat', 'upload/books/cover/84d9313e91bbef1151739930b36af172.jpg', 'upload/books/sound/84d9313e91bbef1151739930b36af172.mp3', 1, 'kids-book-1', '2022-02-03 01:47:05', '2022-02-16 04:47:13');

-- --------------------------------------------------------

--
-- Table structure for table `kids_tbl_page`
--

CREATE TABLE `kids_tbl_page` (
  `id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `page_number` int(11) NOT NULL,
  `image_url` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kids_tbl_page`
--

INSERT INTO `kids_tbl_page` (`id`, `book_id`, `page_number`, `image_url`, `created_at`, `updated_at`) VALUES
(100, 12, 1, 'upload/books/pages/13/Shalat%20Hal%200b.jpg', '0000-00-00 00:00:00', '2022-02-16 05:06:36'),
(101, 12, 2, 'upload/books/pages/13/Shalat%20Hal%200c.jpg', '0000-00-00 00:00:00', '2022-02-16 05:06:41'),
(102, 12, 3, 'upload/books/pages/13/Shalat%20Hal%201.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 12, 4, 'upload/books/pages/13/Shalat%20Hal%202.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 12, 5, 'upload/books/pages/13/Shalat%20Hal%203.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 12, 6, 'upload/books/pages/13/Shalat%20Hal%204.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 12, 7, 'upload/books/pages/13/Shalat%20Hal%205.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 12, 8, 'upload/books/pages/13/Shalat%20Hal%206.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 12, 9, 'upload/books/pages/13/Shalat%20Hal%207.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 12, 10, 'upload/books/pages/13/Shalat%20Hal%208.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 12, 11, 'upload/books/pages/13/Shalat%20Hal%209.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 12, 12, 'upload/books/pages/13/Shalat%20Hal%2010.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 12, 13, 'upload/books/pages/13/Shalat%20Hal%2011.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 12, 14, 'upload/books/pages/13/Shalat%20Hal%2012.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 12, 15, 'upload/books/pages/13/Shalat%20Hal%2013.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 12, 16, 'upload/books/pages/13/Shalat%20Hal%2014.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 12, 17, 'upload/books/pages/13/Shalat%20Hal%2015.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 12, 18, 'upload/books/pages/13/Shalat%20Hal%2016.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 12, 19, 'upload/books/pages/13/Shalat%20Hal%2017.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 12, 20, 'upload/books/pages/13/Shalat%20Hal%2018.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 12, 21, 'upload/books/pages/13/Shalat%20Hal%2019.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 12, 22, 'upload/books/pages/13/Shalat%20Hal%2020.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 12, 23, 'upload/books/pages/13/Shalat%20Hal%2021.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, 12, 24, 'upload/books/pages/13/Shalat%20Hal%2022.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 12, 25, 'upload/books/pages/13/Shalat%20Hal%2023.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, 12, 26, 'upload/books/pages/13/Shalat%20Hal%2024.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, 12, 27, 'upload/books/pages/13/Shalat%20Hal%2025.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 12, 28, 'upload/books/pages/13/Shalat%20Hal%2026.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, 12, 29, 'upload/books/pages/13/Shalat%20Hal%2027.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, 12, 30, 'upload/books/pages/13/Shalat%20Hal%2028.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, 12, 31, 'upload/books/pages/13/Shalat%20Hal%2029.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, 12, 32, 'upload/books/pages/13/Shalat%20Hal%2030.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, 12, 33, 'upload/books/pages/13/Shalat%20Hal%2031.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, 12, 34, 'upload/books/pages/13/Shalat%20Hal%2032.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, 12, 35, 'upload/books/pages/13/Shalat%20Hal%2033.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, 12, 36, 'upload/books/pages/13/Shalat%20Hal%2034.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, 12, 37, 'upload/books/pages/13/Shalat%20Hal%2035.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, 12, 38, 'upload/books/pages/13/Shalat%20Hal%2036.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, 12, 39, 'upload/books/pages/13/Shalat%20Hal%2037.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, 12, 40, 'upload/books/pages/13/Shalat%20Hal%2038.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, 12, 41, 'upload/books/pages/13/Shalat%20Hal%2039.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, 12, 42, 'upload/books/pages/13/Shalat%20Hal%2040.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, 12, 43, 'upload/books/pages/13/Shalat%20Hal%2041.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, 12, 44, 'upload/books/pages/13/Shalat%20Hal%2042.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, 12, 45, 'upload/books/pages/13/Shalat%20Hal%2043.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, 12, 46, 'upload/books/pages/13/Shalat%20Hal%2044.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, 12, 47, 'upload/books/pages/13/Shalat%20Hal%2045.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, 12, 48, 'upload/books/pages/13/Shalat%20Hal%2046.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, 12, 49, 'upload/books/pages/13/Shalat%20Hal%2047.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, 12, 50, 'upload/books/pages/13/Shalat%20Hal%2048.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, 12, 51, 'upload/books/pages/13/Shalat%20Hal%2049.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, 12, 52, 'upload/books/pages/13/Shalat%20Hal%2050.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, 12, 53, 'upload/books/pages/13/Shalat%20Hal%2051.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, 12, 54, 'upload/books/pages/13/Shalat%20Hal%2052.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, 12, 55, 'upload/books/pages/13/Shalat%20Hal%2053.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, 12, 56, 'upload/books/pages/13/Shalat%20Hal%2054.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, 12, 57, 'upload/books/pages/13/Shalat%20Hal%2055.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, 12, 58, 'upload/books/pages/13/Shalat%20Hal%2056.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, 12, 59, 'upload/books/pages/13/Shalat%20Hal%2057.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, 12, 60, 'upload/books/pages/13/Shalat%20Hal%2058.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, 12, 61, 'upload/books/pages/13/Shalat%20Hal%2059.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, 12, 62, 'upload/books/pages/13/Shalat%20Hal%2060.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, 12, 63, 'upload/books/pages/13/Shalat%20Hal%2061.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, 12, 64, 'upload/books/pages/13/Shalat%20Hal%2062.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, 12, 65, 'upload/books/pages/13/Shalat%20Hal%2063.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, 12, 66, 'upload/books/pages/13/Shalat%20Hal%2064.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(166, 12, 67, 'upload/books/pages/13/Shalat%20Hal%2065.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(167, 12, 68, 'upload/books/pages/13/Shalat%20Hal%2066.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(168, 12, 69, 'upload/books/pages/13/Shalat%20Hal%2067.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(169, 12, 70, 'upload/books/pages/13/Shalat%20Hal%2068.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(170, 12, 71, 'upload/books/pages/13/Shalat%20Hal%2069.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(171, 12, 72, 'upload/books/pages/13/Shalat%20Hal%2070.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(172, 12, 73, 'upload/books/pages/13/Shalat%20Hal%2071.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(173, 12, 74, 'upload/books/pages/13/Shalat%20Hal%2072.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(174, 12, 75, 'upload/books/pages/13/Shalat%20Hal%2073.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(175, 12, 76, 'upload/books/pages/13/Shalat%20Hal%2074.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(176, 12, 77, 'upload/books/pages/13/Shalat%20Hal%2075.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(177, 12, 78, 'upload/books/pages/13/Shalat%20Hal%2076.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(178, 12, 79, 'upload/books/pages/13/Shalat%20Hal%2077.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(179, 12, 80, 'upload/books/pages/13/Shalat%20Hal%2078.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(180, 12, 81, 'upload/books/pages/13/Shalat%20Hal%2079.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(181, 12, 82, 'upload/books/pages/13/Shalat%20Hal%2080.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(182, 12, 83, 'upload/books/pages/13/Shalat%20Hal%2081.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(183, 12, 84, 'upload/books/pages/13/Shalat%20Hal%2082.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(184, 12, 85, 'upload/books/pages/13/Shalat%20Hal%2083.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(185, 12, 86, 'upload/books/pages/13/Shalat%20Hal%2084.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(186, 12, 87, 'upload/books/pages/13/Shalat%20Hal%2085.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(187, 12, 88, 'upload/books/pages/13/Shalat%20Hal%2086.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(188, 12, 89, 'upload/books/pages/13/Shalat%20Hal%2087.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(189, 12, 90, 'upload/books/pages/13/Shalat%20Hal%2088.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(190, 12, 91, 'upload/books/pages/13/Shalat%20Hal%2089.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(191, 12, 92, 'upload/books/pages/13/Shalat%20Hal%2090.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(192, 12, 93, 'upload/books/pages/13/Shalat%20Hal%2091.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(193, 12, 94, 'upload/books/pages/13/Shalat%20Hal%2092.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(194, 12, 95, 'upload/books/pages/13/Shalat%20Hal%2093.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(195, 12, 96, 'upload/books/pages/13/Shalat%20Hal%2094.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(196, 12, 97, 'upload/books/pages/13/Shalat%20Hal%2095.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(197, 12, 98, 'upload/books/pages/13/Shalat%20Hal%2096.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(198, 12, 99, 'upload/books/pages/13/Shalat%20Hal%2097.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(199, 12, 100, 'upload/books/pages/13/Shalat%20Hal%2098.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(200, 12, 101, 'upload/books/pages/13/Shalat%20Hal%2099.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(201, 12, 102, 'upload/books/pages/13/Shalat%20Hal%20100.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(202, 12, 103, 'upload/books/pages/13/Shalat%20Hal%20101.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(203, 12, 104, 'upload/books/pages/13/Shalat%20Hal%20102.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(204, 12, 105, 'upload/books/pages/13/Shalat%20Hal%20103.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(205, 12, 106, 'upload/books/pages/13/Shalat%20Hal%20104.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(206, 12, 107, 'upload/books/pages/13/Shalat%20Hal%20105.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(207, 12, 108, 'upload/books/pages/13/Shalat%20Hal%20106.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(208, 12, 109, 'upload/books/pages/13/Shalat%20Hal%20107.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(209, 12, 110, 'upload/books/pages/13/Shalat%20Hal%20108.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `kids_tbl_sound`
--

CREATE TABLE `kids_tbl_sound` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `orders` int(11) NOT NULL,
  `sound_url` varchar(256) NOT NULL,
  `type` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `master_role`
--

CREATE TABLE `master_role` (
  `id` int(11) NOT NULL,
  `level` int(11) NOT NULL DEFAULT 1,
  `name` varchar(24) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_role`
--

INSERT INTO `master_role` (`id`, `level`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', '2022-02-03 03:25:46', '2022-02-03 03:25:46');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `role_id` int(11) NOT NULL,
  `gender` int(1) NOT NULL DEFAULT 1,
  `phone` varchar(16) DEFAULT NULL,
  `username` varchar(16) NOT NULL,
  `password` varchar(256) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `name`, `role_id`, `gender`, `phone`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 1, 1, NULL, 'admin', '$2y$10$l229FjACMFraAbxnDjoQY.FUvtSIkBKkzmi3fMMZfLBBV.IXdwYke', '2022-02-03 03:26:24', '2022-02-03 03:26:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kids_master_book`
--
ALTER TABLE `kids_master_book`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD UNIQUE KEY `number` (`number`);

--
-- Indexes for table `kids_tbl_page`
--
ALTER TABLE `kids_tbl_page`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `page_number` (`page_number`),
  ADD KEY `book_page` (`book_id`);

--
-- Indexes for table `kids_tbl_sound`
--
ALTER TABLE `kids_tbl_sound`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_sound` (`page_id`);

--
-- Indexes for table `master_role`
--
ALTER TABLE `master_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `user_role` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kids_master_book`
--
ALTER TABLE `kids_master_book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `kids_tbl_page`
--
ALTER TABLE `kids_tbl_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=210;

--
-- AUTO_INCREMENT for table `kids_tbl_sound`
--
ALTER TABLE `kids_tbl_sound`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `master_role`
--
ALTER TABLE `master_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kids_tbl_page`
--
ALTER TABLE `kids_tbl_page`
  ADD CONSTRAINT `book_page` FOREIGN KEY (`book_id`) REFERENCES `kids_master_book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kids_tbl_sound`
--
ALTER TABLE `kids_tbl_sound`
  ADD CONSTRAINT `page_sound` FOREIGN KEY (`page_id`) REFERENCES `kids_tbl_page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD CONSTRAINT `user_role` FOREIGN KEY (`role_id`) REFERENCES `master_role` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
