@extends('template.master')
@section('konten')
    <div class="content-wrapper">
        <div class="data-table-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 box-margin">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h4 class="card-title mb-2"> Pengaturan</h4>
                                        <p class="text-muted font-14 mb-4"></p>
                                    </div>
                                </div>
                                <div>
                                    <div class="form-group">
                                        <label for="nip">Jumlah pekerja tiap shift :</label>
                                        <input type="number" class="form-control" id="person" name="person"  placeholder="Masukkan jumlah pegawai">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Jam kerja shift pagi :</label>
                                        <input type="time" id="first_shift_time">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Jam kerja shift sore :</label>
                                        <input type="time" id="second_shift_time">
                                    </div>

                                    <button id="submit" class="btn btn-primary float-right">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $( document ).ready(function() {
            $("#select_roles").html();
            userId = null;
            <?php
            if(isset($user_id)) echo "userId = ".$user_id.";"
            ?>
            $.get("/api/master/rules").done(function (data, status) {
                $("#person").val(data.data.person_per_shift);
                $("#first_shift_time").val(data.data.first_shift_time);
                $("#second_shift_time").val(data.data.second_shift_time);
            });

            $("#submit").click(function (){
                $.post("/api/master/rules/edit",{
                    person_per_shift : $("#person").val(),
                    first_shift_time : $("#first_shift_time").val(),
                    second_shift_time : $("#second_shift_time").val(),
                }).done(function (data, status) {
                    window.location.href = "/home";
                }).fail(function (XMLHttpRequest, textStatus, errorThrown){
                    if(XMLHttpRequest.status !== 401){
                        alert(XMLHttpRequest.responseJSON.message);
                    } else {
                        window.location.href = "/";
                    }
                });
            })
        });
    </script>
@endsection
