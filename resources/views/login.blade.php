<!doctype html>
<html lang="en">


<!-- Mirrored from demo.riktheme.com/xvito/side-menu/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 03 Sep 2020 07:14:45 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Required meta tags -->

    <title>Admin Login</title>

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('assets/template/img/core-img/favicon.png') }}">

    <!-- Master Stylesheet CSS -->
    <link rel="stylesheet" href="{{ asset('assets/template/style.css') }}">

</head>

<body class="login-area">

    <!-- Preloader -->
    <div id="preloader-area">
        <div class="lds-ripple">
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- Preloader -->

    <!-- ======================================
    ******* Page Wrapper Area Start **********
    ======================================= -->
    <div class="main-content- h-100vh">
        <div class="container h-100">
            <div class="row h-100 align-items-center justify-content-center">
                <div class="hero">
                    <div class="cube"></div>
                    <div class="cube"></div>
                    <div class="cube"></div>
                    <div class="cube"></div>
                    <div class="cube"></div>
                    <div class="cube"></div>
                </div>
                <div class="col-sm-10 col-md-8 col-lg-5">
                    <!-- Middle Box -->
                    <div class="middle-box">
                        <div class="card">
                            <div class="card-body p-4">

                                <!-- Logo -->
                                <h4 class="font-24 mb-30">Login</h4>

                                <form action="/admin/post-login" method="POST">
                                {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="input-username">Username</label><input id="input-username" class="form-control login" type="text" name="username" required="" placeholder="Masukkan username">
                                    </div>

                                    <div class="form-group">
                                        <label for="input-password">Password</label><input id="input-password" class="form-control login" type="password" required="" name="password" placeholder="Masukkan password">
                                    </div>

                                    <div class="form-group d-flex justify-content-between align-items-center mb-3">
                                        <div class="checkbox d-inline mb-0">
                                            <input type="checkbox" name="checkbox-1" id="checkbox-8">
                                            <label for="checkbox-8" class="cr mb-0 font-13">Remember me</label>
                                        </div>
                                    </div>

                                    <div class="form-group mb-0">
                                        <input id="btn-submit" type="button" class="btn btn-primary btn-block" value="Login"/>
                                    </div>

                                </form>

                                <!-- end card -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ======================================
    ********* Page Wrapper Area End ***********
    ======================================= -->

    <!-- Plugins Js -->
    <script src="{{ asset('assets/template/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/bundle.js') }}"></script>

    <!-- Active JS -->
    <script src="{{ asset('assets/template/js/default-assets/active.js') }}"></script>
</body>
<script>

    $( document ).ready(function() {

        console.log( "ready!" );
        $('#preloader-area').fadeOut('slow', function () {
            $(this).remove();
        });

        $('#btn-submit').click(function (){
            $.post("/api/login",{
                username : $("#input-username").val(),
                password : $("#input-password").val()
            }).done(function (data, status) {
                localStorage.setItem('token', data.data.token);
                window.location.href = "/kids"
            }).fail(function (XMLHttpRequest, textStatus, errorThrown){
                alert(XMLHttpRequest.responseJSON.message);
            });
        })
    });
</script>

</html>
