@extends('template.master')
@section('konten')
<div class="content-wrapper">
    <div class="data-table-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h2 id="book-title" class="card-title mb-2"></h2>
                            <div class="d-flex justify-content-center">
                                <div id="loading-page"  class="spinner-border text-primary m-5" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>
                            <div id="content-page">
                                <form id="form-update-page">
                                    <table class="table dt-responsive w-200">
                                        <tbody id="body-detail-table">
                                            <tr>
                                                <th class="align-middle">Halaman</th>
                                                <th><input name="page_number" type="number" class="form-control" id="page-number"></th>
                                            </tr>
                                            <tr>
                                                <th class="align-middle">Gambar halaman</th>
                                                <th>
                                                    <div id="page-image"></div><br>
                                                    <input name="image" type="file" class="form-control" id="page-image-input">
                                                </th>
                                            </tr>
                                            <tr>
                                                <th colspan="2">
                                                    <div class="row w-100 m-0">
                                                        <div class="col-sm-8 align-middle">
                                                            <div id="notif-update-page" class="text-break m-0"></div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <button id="form-submit" type="submit" class="float-right btn btn-success">Update</button>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>
                                <h6 class="text-secondary">Bagian Angka</h6>
                                <table class="table dt-responsive w-200">
                                    <thead>
                                        <tr>
                                            <th style='width: 1px;text-align: center'>Urutan</th>
                                            <th style='width: 1px;text-align: center'>Audio</th>
                                            <th style='width: 1px;text-align: center'></th>
                                            <th style='width: 1px;text-align: center'></th>
                                        </tr>
                                    </thead>

                                    <tbody id="body-table">

                                    </tbody>
                                </table>
                                <br>
                                <h6 class="text-secondary">Bagian Alphabet</h6>
                                <table class="table dt-responsive w-200">
                                    <thead >
                                    <tr>
                                        <th style='width: 1px;text-align: center'>Urutan</th>
                                        <th style='width: 1px;text-align: center'>Audio</th>
                                        <th style='width: 1px;text-align: center'></th>
                                        <th style='width: 1px;text-align: center'></th>
                                    </tr>
                                    </thead>

                                    <tbody id="body-table-2">

                                    </tbody>
                                </table>
                            </div>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
                <div class="col-12 box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h2 id="book-title" class="card-title mb-2">Tambah Audio Baru</h2>
                            <form id="form-create-sound">
                                <table class="table dt-responsive w-200">
                                    <tbody id="body-detail-table">

                                    <tr>
                                        <th class="align-middle">Urutan</th>
                                        <th><input name="orders" type="number" class="form-control" id="sound-order"></th>
                                    </tr>
                                    <tr>
                                        <th class="align-middle"><label for="sound-type">Tipe</label></th>
                                        <th>
                                            <select name="type" class="form-control" id="sound-type">
                                                <option value="1">Bagian angka</option>
                                                <option value="2">Bagian alphabet</option>
                                            </select>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="align-middle">Sound</th>
                                        <th>
                                            <input name="sound" type="file" class="form-control" id="page-image-input">
                                        </th>
                                    </tr>

                                    <tr>
                                        <th colspan="2">
                                            <div class="row w-100 m-0">
                                                <div class="col-sm-8 align-middle">
                                                    <div id="notif-create-sound" class="text-break m-0"></div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <button id="form-submit-create" type="submit" class="float-right btn btn-success">Tambah</button>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="sounds-modal" tabindex="-1" role="dialog" aria-labelledby="soundsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form id="form-update-sound">
                        <table class="table dt-responsive w-200">
                            <tbody id="body-detail-table">
                                <tr>
                                    <th class="align-middle"><label for="sound-order">Urutan</label></th>
                                    <th><input name="orders" type="number" class="form-control" id="sound-order"></th>
                                </tr>
                                <tr>
                                    <th class="align-middle"><label for="sound-input">Audio</label></th>
                                    <th><input name="sound" type="file" class="form-control" id="sound-input"></th>
                                </tr>
                                <tr>
                                    <th colspan="2">
                                        <div class="row w-100 m-0">
                                            <div class="col-sm-8 align-middle">
                                                <div id="notif-update-sound" class="text-break m-0"></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <button id="form-submit-update-sound" type="submit" class="float-right btn btn-success">Update</button>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
    var pageDetail = null;
    var currentSoundId = null;
    $( document ).ready(function() {
        getPageDetail();
        initFormCreateSound();
        initFormUpdatePage();
        initFormUpdateSound();
    });
    function getPageDetail(){
        const contentPage = $("#content-page");
        const loadingPage = $("#loading-page");
        contentPage.hide();
        loadingPage.show();

        $.get("/api/kids/book/page/detail/"+{{$page_id}}).done(function (data, sts) {
            $("#body-table-2").html("");
            $("#body-table").html("");
            $("#book-title").html(data.data.book.name);
            contentPage.show();
            loadingPage.hide();
            implPageDetail(data);
            pageDetail = data.data;
            var type1 = 0
            var type2 = 0
            data.data.sounds.forEach(function (item, index) {
                if(item.type === 1) {
                    type1++;
                    $("#body-table").append(
                        "<tr onclick=\"\">" +
                        "<td style='width: 1px;text-align: center'>" + item.orders + "</td>" +
                        "<td style='width: 1px;text-align: center'>"+getAudioPlayer(item.sound_url, "sound"+item.id)+"</td>" +
                        "<td style='width: 1px;text-align: center'><button class='btn btn-xs btn-primary' onclick='openSoundModal("+index+")'>Edit</button></td>" +
                        "<td style='width: 1px;text-align: center'><button onclick='deleteSound(this,"+item.id+")' class='btn btn-xs btn-danger'>Hapus</button></td>"+
                        "</tr>"
                    );
                } else {
                    type2++;
                    $("#body-table-2").append(
                        "<tr onclick=\"\">" +
                        "<td style='width: 1px;text-align: center'>" + item.orders + "</td>" +
                        "<td style='width: 1px;text-align: center'>"+getAudioPlayer(item.sound_url, "sound"+item.id)+"</td>" +
                        "<td style='width: 1px;text-align: center'><button class='btn btn-xs btn-primary' onclick='openSoundModal("+index+");'>Edit</button></td>" +
                        "<td style='width: 1px;text-align: center'><button onclick='deleteSound(this,"+item.id+")' class='btn btn-xs btn-danger'>Hapus</button></td>"+
                        "</tr>"
                    );
                }
            });
            if(type1 === 0){
                $("#body-table").append("<td colspan='4' style='width: 1px;text-align: center'>Belum ada data</td>")
            }
            if(type2 === 0){
                $("#body-table-2").append("<td colspan='4' style='width: 1px;text-align: center'>Belum ada data</td>")
            }
        });
    }

    function initFormUpdatePage(){
        const formUpdatePage = $("#form-update-page");
        formUpdatePage.on('submit', function (e) {
            e.preventDefault();
            const button = $('#form-submit');
            const notifUpdatePage = $('#notif-update-page');
            notifUpdatePage.html("");
            button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
            button.attr("disabled", true);
            $.ajax({
                type: 'POST',
                url: '/api/admin/kids/book/page/update/'+pageDetail.id,
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                    button.attr("disabled", true);
                },
                success: function (data) {
                    formUpdatePage.trigger('reset');
                    getPageDetail();
                    notifUpdatePage.html("<p class='text-success m-1'>Page updated successfully</p>")

                },
                error: function (data) {
                    const errors = data.responseJSON.errors;
                    Object.keys(data.responseJSON.errors).forEach((item,index)=>{
                        notifUpdatePage.append("<p class='text-danger m-1'>"+errors[item][0]+"</p>")

                    })
                },
                complete: function (data) {
                    // implPageDetail(data);
                    button.html("Update");
                    button.removeAttr("disabled");
                }
            })

            return false;
        });
    }
    function initFormCreateSound() {
        const formCreateSound = $("#form-create-sound");
        formCreateSound.on('submit', function (e) {
            e.preventDefault();
            const button = $('#form-submit-create');
            const data = new FormData(this);
            const notifCreateSound = $('#notif-create-sound');
            notifCreateSound.html("");
            data.set("page_id", {{$page_id}});
            $.ajax({
                type: 'POST',
                url: '/api/admin/kids/book/page/sound/create',
                data: data,
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                    button.attr("disabled", true);
                },
                success: function (data) {
                    formCreateSound.trigger('reset');
                    getPageDetail();
                    notifCreateSound.html("<p class='text-success m-1'>Sound created successfully</p>")

                },
                error: function (data) {
                    const errors = data.responseJSON.errors;
                    Object.keys(data.responseJSON.errors).forEach((item,index)=>{
                        notifCreateSound.append("<p class='text-danger m-1'>"+errors[item][0]+"</p>")

                    })
                },
                complete: function (data) {
                    // implPageDetail(data);
                    button.html("Tambah");
                    button.removeAttr("disabled");
                }
            })
        });
    }
    function initFormUpdateSound(){
        const modal = $('#sounds-modal');
        const formUpdateSound = modal.find("#form-update-sound");
        const notifUpdateSound = modal.find('#notif-update-sound');
        notifUpdateSound.html("");
        formUpdateSound.on('submit', function (e) {
            e.preventDefault();
            const button = modal.find('#form-submit-update-sound');
            button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
            button.attr("disabled", true);
            $.ajax({
                type: 'POST',
                url: '/api/admin/kids/book/page/sound/update/'+currentSoundId,
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                    button.attr("disabled", true);
                },
                success: function (data) {
                    formUpdateSound.trigger('reset');
                    modal.modal('hide');
                    getPageDetail();
                },
                error: function (data) {
                    const errors = data.responseJSON.errors;
                    Object.keys(data.responseJSON.errors).forEach((item,index)=>{
                        notifUpdateSound.append("<p class='text-danger m-1'>"+errors[item][0]+"</p>")
                    })
                },
                complete: function (data) {
                    // implPageDetail(data);
                    button.html("Update");
                    button.removeAttr("disabled");
                }
            })

            return false;
        });
    }
    function implPageDetail(data){
        $("#page-number").val(data.data.page_number);
        var openImageCover = "openImageModal(\"{{url("")}}/"+data.data.image_url+"\");";
        $("#page-image").html("<button type='button' class='btn btn-xs btn-primary' onClick='"+openImageCover+"'>Lihat</td>");
    }
    function openSoundModal(index){
        const item = pageDetail.sounds[index];
        const modal = $('#sounds-modal');
        modal.modal('show');
        modal.find("#sound-order").val(item.orders);
        modal.find('#notif-update-sound').html("");
        currentSoundId = item.id;
    }
    function deleteSound(btn, id){
        if(confirm("Anda yakin ingin menghapus ini?")) {
            var button = $(btn);
            $.ajax({
                type: 'DELETE',
                url: '/api/admin/kids/book/page/sound/delete/' + id,
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                    button.attr("disabled", true);
                },
                success: function (data) {
                    getPageDetail();
                },
                error: function (data) {
                    const errors = data.responseJSON.message;
                    alert(errors);
                    // implPageDetail(data);
                    button.html("Hapus");
                    button.removeAttr("disabled");
                },
                complete: function (data) {

                }
            })
        }
    }

</script>
@endsection
