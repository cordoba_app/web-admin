@extends('template.master')
@section('konten')
<div class="content-wrapper">
    <div class="data-table-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 box-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h4 class="card-title m-0">Pengaturan Aplikasi AACI</h4>
                                </div>
                            </div>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
                <div class="col-12 box-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="row  mb-2">
                                <div class="col-sm-6">
                                    <h4 class="card-title">Tentang Aplikasi</h4>
                                    <form id="form-change-about">
                                        <table class="table dt-responsive w-200">
                                            <tbody id="body-detail-table">
                                            <tr>
                                                <th><textarea name="about" class="form-control" id="about"></textarea></th>
                                            </tr>
                                                <tr>
                                                    <th>
                                                        <div class="row w-100 m-0">

                                                            <div class="col-sm-8 align-middle">
                                                                <div id="notif-about" class="text-break m-0"></div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <button id="form-submit-about" type="submit" class="float-right btn btn-success">Submit</button>
                                                            </div>
                                                        </div>
                                                    </th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                                <div class="col-sm-6">
                                    <h4 class="card-title">Petunjuk penggunaan</h4>
                                    <form id="form-change-guide">
                                        <table class="table dt-responsive w-200">
                                            <tbody id="body-detail-table">
                                                <tr>
                                                    <th><textarea name="guide" class="form-control" id="guide"></textarea></th>
                                                </tr>
                                                <tr>
                                                    <th colspan="2">
                                                        <div class="row w-100 m-0">

                                                            <div class="col-sm-8 align-middle">
                                                                <div id="notif-guide" class="text-break m-0"></div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <button id="form-submit-guide" type="submit" class="float-right btn btn-success">Submit</button>
                                                            </div>
                                                        </div>
                                                    </th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>

                                </div>
                            </div>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
        </div>
    </div>
</div>
<script>
    $( document ).ready(function() {

        $.get("/api/kids/information").done(function (data, sts) {
            $("#about").html(data.data.about)
            $("#guide").html(data.data.guide)
        })
        initFormAbout();
        initFormGuide();
    });
    function initFormAbout(){
        const formChangeAbout = $("#form-change-about");
        formChangeAbout.on('submit', function (e) {
            e.preventDefault();
            const button = $('#form-submit-about');
            const notif = $('#notif-about');
            notif.html("");
            button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
            button.attr("disabled", true);
            $.ajax({
                type: 'POST',
                url: '/api/admin/kids/information/about',
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                    button.attr("disabled", true);
                },
                success: function (data) {
                    notif.html("<p class='text-success m-1'>About Application changed successfully</p>")

                },
                error: function (data) {
                    const errors = data.responseJSON.errors;
                    Object.keys(data.responseJSON.errors).forEach((item,index)=>{
                        notif.append("<p class='text-danger m-1'>"+errors[item][0]+"</p>")

                    })

                },
                complete: function (data) {
                    // implPageDetail(data);
                    button.html("Submit");
                    button.removeAttr("disabled");
                }
            })

            return false;
        });

    }

    function initFormGuide(){
        const formChangeGuide = $("#form-change-guide");
        formChangeGuide.on('submit', function (e) {
            e.preventDefault();
            const button = $('#form-submit-guide');
            const notif = $('#notif-guide');
            notif.html("");
            button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
            button.attr("disabled", true);
            $.ajax({
                type: 'POST',
                url: '/api/admin/kids/information/guide',
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                    button.attr("disabled", true);
                },
                success: function (data) {
                    notif.html("<p class='text-success m-1'>Application Guide changed successfully</p>")

                },
                error: function (data) {
                    const errors = data.responseJSON.errors;
                    Object.keys(data.responseJSON.errors).forEach((item,index)=>{
                        notif.append("<p class='text-danger m-1'>"+errors[item][0]+"</p>")

                    })

                },
                complete: function (data) {
                    // implPageDetail(data);
                    button.html("Submit");
                    button.removeAttr("disabled");
                }
            })

            return false;
        });

    }


</script>
@endsection
