@extends('template.master')
@section('konten')
<div class="content-wrapper">
    <div class="data-table-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h2 class="card-title mb-2">Buku Ajarkan Aku Cinta Islam</h2>
                            <div class="d-flex justify-content-center">
                                <div id="loading-books"  class="spinner-border text-primary m-5" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>

                            <table id="table-books" class="table dt-responsive w-200">
                                <thead >
                                    <tr>
                                        <th style='width: 1px;text-align: center'>Nomor</th>
                                        <th style='min-width: 200px;text-align: center'>Judul buku</th>
                                        <th style='width: 1px;text-align: center'>Suara Cover</th>
                                        <th style='width: 1px;text-align: center'>Cover</th>
                                        <th style='width: 1px;text-align: center'></th>
                                        <th style='width: 1px;text-align: center'></th>
                                    </tr>
                                </thead>

                                <tbody id="body-table">

                                </tbody>
                            </table>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
                <div class="col-12 box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h2 id="title" class="card-title mb-2">Tambah buku baru</h2>
                            <form id="form-create-book">
                                <table class="table dt-responsive w-200">
                                    <tbody id="body-detail-table">
                                    <tr>
                                        <th class="align-middle">Judul buku</th>
                                        <th><input name="name" type="text" class="form-control" id="book-title"></th>
                                    </tr>
                                    <tr>
                                        <th class="align-middle">Nomor buku</th>
                                        <th><input name="number" type="number" class="form-control" id="book-number"></th>
                                    </tr>
                                    <tr>
                                        <th class="align-middle">Kode buku</th>
                                        <th><input name="code" type="text" class="form-control" id="book-code"></th>
                                    </tr>
                                    <tr>
                                        <th class="align-middle">Halaman Pendahuluan</th>
                                        <th><input name="prepage" type="text" class="form-control" id="book-prepage"></th>
                                    </tr>
                                    <tr>
                                        <th class="align-middle">Cover buku</th>
                                        <th>
                                            <input name="cover" type="file" class="form-control" id="book-cover-image-input">
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="align-middle">Suara cover</th>
                                        <th>
                                            <input name="cover_sound" type="file" class="form-control" id="book-cover-sound-input">
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="2">
                                            <div class="row w-100 m-0">
                                                <div class="col-sm-8 align-middle">
                                                    <div id="notif-create-book" class="text-break m-0"></div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <button id="form-submit" type="submit" class="float-right btn btn-success">Tambah</button>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>

                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
        </div>
    </div>
</div>
    <script>
        $( document ).ready(function() {
            getBooks();
            initFormCreateBook();
        });
        function getBooks(){
            $("#body-table").html("");
            const tableBook = $("#table-books");
            const loadingBook = $("#loading-books");
            tableBook.hide();
            loadingBook.show();
            $.get("/api/kids/book/all").done(function (data, sts) {
                console.log(data);
                tableBook.show();
                loadingBook.hide();
                if(data.data.length === 0){
                    $("#body-table").append("<tr><td colspan='4' style='width: 1px;text-align: center'>Belum ada data</td></tr>")
                }
                data.data.forEach(function (item, index) {
                    var openImage = "openImageModal(\"{{url("")}}/"+item.cover_url+"\");";
                    $("#body-table").append(
                        "<tr>"+
                        "<td style='width: 1px;text-align: center'>"+item.number+"</td>"+
                        "<td style='width: 1px;text-align: center'>"+item.name+"</td>"+
                        "<td style='width: 1px;text-align: center'>"+getAudioPlayer(item.cover_sound_url, "sound-"+item.id)+"</td>"+
                        "<td style='width: 1px;text-align: center'><button class='btn btn-xs btn-primary' onClick='"+openImage+"'>Lihat</button></td>"+
                        "<td style='width: 1px;text-align: center'><a class='btn btn-xs btn-success' href='/kids/book/"+item.id+"'>Edit</a></td>"+
                        "<td style='width: 1px;text-align: center'><button onclick='deleteBook(this,"+item.id+")' class='btn btn-xs btn-danger'>Hapus</button></td>"+
                        "</tr>"

                    );
                });

            })
        }
        function deleteBook(btn, id){
            if(confirm("Anda yakin ingin menghapus ini?")) {

                var button = $(btn);
                $.ajax({
                    type: 'DELETE',
                    url: '/api/admin/kids/book/delete/' + id,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                        button.attr("disabled", true);
                    },
                    success: function (data) {
                        getBooks();
                    },
                    error: function (data) {
                        const errors = data.responseJSON.message;
                        alert(errors);
                        // implPageDetail(data);
                        button.html("Hapus");
                        button.removeAttr("disabled");
                    },
                    complete: function (data) {

                    }
                })
            }
        }

        function initFormCreateBook(){

            const formCreateBook = $("#form-create-book");
            formCreateBook.on('submit', function(e) {
                e.preventDefault();
                const notifCreateBook = $('#notif-create-book');
                notifCreateBook.html("");
                const button = $('#form-submit');

                $.ajax({
                    type: 'POST',
                    url: '/api/admin/kids/book/create',
                    data: new FormData(this),
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                        button.attr("disabled", true);
                    },
                    success: function (data){
                        formCreateBook.trigger('reset');
                        getBooks();
                        notifCreateBook.html("<p class='text-success m-1'>Book created successfully</p>")

                    },
                    error: function (data){
                        const errors = data.responseJSON.errors;
                        Object.keys(data.responseJSON.errors).forEach((item,index)=>{
                            notifCreateBook.append("<p class='text-danger m-1'>"+errors[item][0]+"</p>")

                        })
                    },
                    complete: function (data) {
                        // implPageDetail(data);
                        button.html("Tambah");
                        button.removeAttr("disabled");
                    }
                })
            });
        }


    </script>
@endsection
