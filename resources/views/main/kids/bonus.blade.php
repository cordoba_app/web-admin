@extends('template.master')
@section('konten')
<div class="content-wrapper">
    <div class="data-table-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h2 id="book-title" class="card-title mb-2">Ular Tangga - Pertanyaan & Jawaban</h2>
                            <div class="d-flex justify-content-center">
                                <div id="loading-page"  class="spinner-border text-primary m-5" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>
                            <div id="content-page">
                                <table class="table dt-responsive w-200">
                                    <thead>
                                        <tr>
                                            <th style='width: 1px;text-align: center'>Urutan</th>
                                            <th style='width: 1px;text-align: center'>Pertanyaan</th>
                                            <th style='width: 1px;text-align: center'>Jawaban</th>
                                            <th style='width: 1px;text-align: center'>Warna</th>
                                            <th style='width: 1px;text-align: center'></th>
                                            <th style='width: 1px;text-align: center'></th>
                                        </tr>
                                    </thead>

                                    <tbody id="body-table">

                                    </tbody>
                                </table>

                            </div>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
                <div class="col-12 box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h2 id="book-title" class="card-title mb-2">Tambah Pertanyaan/Jawaban Baru</h2>
                            <form id="form-create-qna">
                                <table class="table dt-responsive w-200">
                                    <tbody id="body-detail-table">

                                    <tr>
                                        <th class="align-middle">Urutan ke</th>
                                        <th><input name="orders" type="number" class="form-control" id="sound-order"></th>
                                    </tr>
                                    <tr>
                                        <th class="align-middle">Pertanyaan</th>
                                        <th>
                                            <input name="question_sound" type="file" class="form-control" id="sound-image-input">
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="align-middle">Jawaban</th>
                                        <th>
                                            <input name="answer_sound" type="file" class="form-control" id="sound-image-input">
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="align-middle">Warna</th>
                                        <th>
                                            <select name="color" class="form-control" id="sound_color">
                                                <option value="merah">Merah</option>
                                                <option value="biru">Biru</option>
                                                <option value="hijau">Hijau</option>
                                            </select>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="2">
                                            <div class="row w-100 m-0">
                                                <div class="col-sm-8 align-middle">
                                                    <div id="notif-create-qna" class="text-break m-0"></div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <button id="form-submit-create" type="submit" class="float-right btn btn-success">Tambah</button>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="sounds-modal" tabindex="-1" role="dialog" aria-labelledby="soundsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form id="form-update-sound">
                        <table class="table dt-responsive w-200">
                            <tbody id="body-detail-table">
                                <tr>
                                    <th class="align-middle"><label for="sound-order">Urutan</label></th>
                                    <th><input name="orders" type="number" class="form-control" id="sound-order"></th>
                                </tr>
                                <tr>
                                    <th class="align-middle">Pertanyaan</th>
                                    <th>
                                        <input name="question_sound" type="file" class="form-control" id="page-q-input">
                                    </th>
                                </tr>
                                <tr>
                                    <th class="align-middle">Jawaban</th>
                                    <th>
                                        <input name="answer_sound" type="file" class="form-control" id="page-a-input">
                                    </th>
                                </tr>
                                <tr>
                                    <th class="align-middle">Warna</th>
                                    <th>
                                        <select name="color" class="form-control" id="edit_sound_color">
                                            <option value="merah">Merah</option>
                                            <option value="biru">Biru</option>
                                            <option value="hijau">Hijau</option>
                                        </select>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="2">
                                        <div class="row w-100 m-0">
                                            <div class="col-sm-8 align-middle">
                                                <div id="notif-update-sound" class="text-break m-0"></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <button id="form-submit-update-sound" type="submit" class="float-right btn btn-success">Update</button>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
    var bonusSounds = [];
    var currentBonusId = null;
    $( document ).ready(function() {
        getListSound();
        initFormCreateQnA();
        initFormUpdateQnA();
    });
    function getListSound(){
        const contentPage = $("#content-page");
        const loadingPage = $("#loading-page");
        const table = $("#body-table");
        contentPage.hide();
        loadingPage.show();

        $.get("/api/kids/bonus/all").done(function (data, sts) {
            table.html("");
            contentPage.show();
            loadingPage.hide();
            bonusSounds = data.data;
            bonusSounds.forEach(function (item, index) {
                table.append(
                    "<tr onclick=\"\">" +
                    "<td style='width: 1px;text-align: center'>" + item.orders + "</td>" +
                    "<td style='width: 1px;text-align: center'>"+getAudioPlayer(item.question_sound_url, "q-sound-"+item.id)+"</td>" +
                    "<td style='width: 1px;text-align: center'>"+getAudioPlayer(item.answer_sound_url, "a-sound-"+item.id)+"</td>" +
                    "<td style='width: 1px;text-align: center'>" + item.color + "</td>" +
                    "<td style='width: 1px;text-align: center'><button class='btn btn-xs btn-primary' onclick='openSoundModal("+index+")'>Edit</button></td>" +
                    "<td style='width: 1px;text-align: center'><button onclick='deleteQnA(this,"+item.id+")' class='btn btn-xs btn-danger'>Hapus</button></td>"+
                    "</tr>"
                );
            });
            if(bonusSounds.length === 0){
                table.append("<td colspan='4' style='width: 1px;text-align: center'>Belum ada data</td>")
            }

        });
    }


    function initFormCreateQnA() {
        const formCreateQnA = $("#form-create-qna");
        formCreateQnA.on('submit', function (e) {
            e.preventDefault();
            const button = $('#form-submit-create');
            const data = new FormData(this);
            const notifCreateQnA = $('#notif-create-qna');
            notifCreateQnA.html("");
            $.ajax({
                type: 'POST',
                url: '/api/admin/kids/bonus/create',
                data: data,
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                    button.attr("disabled", true);
                },
                success: function (data) {
                    formCreateQnA.trigger('reset');
                    getListSound();
                    notifCreateQnA.html("<p class='text-success m-1'>Sound created successfully</p>")

                },
                error: function (data) {
                    const errors = data.responseJSON.errors;
                    Object.keys(data.responseJSON.errors).forEach((item,index)=>{
                        notifCreateQnA.append("<p class='text-danger m-1'>"+errors[item][0]+"</p>")

                    })
                },
                complete: function (data) {
                    // implPageDetail(data);
                    button.html("Tambah");
                    button.removeAttr("disabled");
                }
            })
        });
    }
    function initFormUpdateQnA(){
        const modal = $('#sounds-modal');
        const formUpdateSound = modal.find("#form-update-sound");
        const notifUpdateSound = modal.find('#notif-update-sound');
        notifUpdateSound.html("");
        formUpdateSound.on('submit', function (e) {
            e.preventDefault();
            const button = modal.find('#form-submit-update-sound');
            button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
            button.attr("disabled", true);
            $.ajax({
                type: 'POST',
                url: '/api/admin/kids/bonus/update/'+currentBonusId,
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                    button.attr("disabled", true);
                },
                success: function (data) {
                    formUpdateSound.trigger('reset');
                    modal.modal('hide');
                    getListSound();
                },
                error: function (data) {
                    const errors = data.responseJSON.errors;
                    Object.keys(data.responseJSON.errors).forEach((item,index)=>{
                        notifUpdateSound.append("<p class='text-danger m-1'>"+errors[item][0]+"</p>")
                    })
                },
                complete: function (data) {
                    // implPageDetail(data);
                    button.html("Update");
                    button.removeAttr("disabled");
                }
            })

            return false;
        });
    }

    function openSoundModal(index){
        const item = bonusSounds[index];
        const modal = $('#sounds-modal');
        modal.modal('show');
        modal.find("#sound-symbol").val(item.symbol);
        modal.find("#sound-order").val(item.orders);
        modal.find('#notif-update-sound').html("");
        modal.find('#edit_sound_color').val(item.color);
        currentBonusId = item.id;
    }
    function deleteQnA(btn, id){
        if(confirm("Anda yakin ingin menghapus ini?")) {
            var button = $(btn);
            $.ajax({
                type: 'DELETE',
                url: '/api/admin/kids/bonus/delete/' + id,
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                    button.attr("disabled", true);
                },
                success: function (data) {
                    getListSound();
                },
                error: function (data) {
                    const errors = data.responseJSON.message;
                    alert(errors);
                    // implPageDetail(data);
                    button.html("Hapus");
                    button.removeAttr("disabled");
                },
                complete: function (data) {

                }
            })
        }
    }

</script>
@endsection
