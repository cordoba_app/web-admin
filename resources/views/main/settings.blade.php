@extends('template.master')
@section('konten')
<div class="content-wrapper">
    <div class="data-table-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 box-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h4 class="card-title m-0">Pengaturan</h4>
                                </div>
                            </div>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
                <div class="col-12 box-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="row  mb-2">
                                <div class="col-sm-6">
                                    <h4 class="card-title">Ubah Username</h4>
                                    <form id="form-change-username">
                                        <table class="table dt-responsive w-200">
                                            <tbody id="body-detail-table">
                                            <tr>
                                                <th class="align-middle">Username</th>
                                                <th><input name="username" type="text" class="form-control" id="admin-username"></th>
                                            </tr>
                                            <tr>
                                                <th colspan="2">
                                                    <div class="row w-100 m-0">

                                                        <div class="col-sm-8 align-middle">
                                                            <div id="notif-username" class="text-break m-0"></div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <button id="form-submit-username" type="submit" class="float-right btn btn-success">Submit</button>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </form>

                                </div>
                                <div class="col-sm-6">

                                <h4 class="card-title">Ubah Password</h4>
                                    <form id="form-change-password">
                                        <table class="table dt-responsive w-200">
                                            <tbody id="body-detail-table">

                                            <tr>
                                                <th class="align-middle">Password</th>
                                                <th><input name="password" type="password" class="form-control" id="book-password"></th>
                                            </tr>
                                            <tr>
                                                <th class="align-middle">Konfirmasi Password</th>
                                                <th><input name="password-confirmation" type="password" class="form-control" id="book-password-confirmation"></th>
                                            </tr>
                                            <tr>
                                                <th colspan="2">
                                                    <div class="row w-100 m-0">

                                                        <div class="col-sm-8 align-middle">
                                                            <div id="notif-password" class="text-break m-0"></div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <button id="form-submit-pass" type="submit" class="float-right btn btn-success">Submit</button>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
        </div>
    </div>
</div>
<script>
    $( document ).ready(function() {

        $.get("/api/admin/me").done(function (data, sts) {
            $("#admin-username").val(data.data.username)
        })
        initFormUsername();
        initFormChangepass();
    });
    function initFormUsername(){
        const formChangeUsername = $("#form-change-username");
        formChangeUsername.on('submit', function (e) {
            e.preventDefault();
            const button = $('#form-submit-username');
            const notifUsername = $('#notif-username');
            notifUsername.html("");
            button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
            button.attr("disabled", true);
            $.ajax({
                type: 'POST',
                url: '/api/admin/change-username',
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                    button.attr("disabled", true);
                },
                success: function (data) {
                    notifUsername.html("<p class='text-success m-1'>Username changed successfully</p>")

                },
                error: function (data) {
                    const errors = data.responseJSON.errors;
                    Object.keys(data.responseJSON.errors).forEach((item,index)=>{
                        notifUsername.append("<p class='text-danger m-1'>"+errors[item][0]+"</p>")

                    })

                },
                complete: function (data) {
                    // implPageDetail(data);
                    button.html("Submit");
                    button.removeAttr("disabled");
                }
            })

            return false;
        });
    }
    function initFormChangepass(){
        const formChangePass = $("#form-change-password");
        formChangePass.on('submit', function (e) {
            e.preventDefault();
            const button = $('#form-submit-pass');
            const notifPassword = $('#notif-password');
            notifPassword.html("");
            button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
            button.attr("disabled", true);
            $.ajax({
                type: 'POST',
                url: '/api/admin/change-password',
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                    button.attr("disabled", true);
                },
                success: function (data) {
                    formChangePass.trigger('reset');
                    notifPassword.html("<p class='text-success m-1'>Password changed successfully</p>")

                },
                error: function (data) {
                    const errors = data.responseJSON.errors;
                    Object.keys(data.responseJSON.errors).forEach((item,index)=>{
                        notifPassword.append("<p class='text-danger m-1'>"+errors[item][0]+"</p>")

                    })
                },
                complete: function (data) {
                    // implPageDetail(data);
                    button.html("Submit");
                    button.removeAttr("disabled");
                }
            })

            return false;
        });
    }


</script>
@endsection
