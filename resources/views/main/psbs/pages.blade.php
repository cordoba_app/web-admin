@extends('template.master')
@section('konten')
<div class="content-wrapper">
    <div class="data-table-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h2 id="title" class="card-title mb-2">Detail buku</h2>
                            <div class="d-flex justify-content-center">
                                <div id="loading-book"  class="spinner-border text-primary m-5" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>
                            <div id="content-book">
                                <form id="form-update-book">
                                    <table class="table dt-responsive w-200">
                                        <tbody id="body-detail-table">
                                        <tr>
                                            <th class="align-middle">Nama buku</th>
                                            <th><input name="name" type="text" class="form-control" id="book-title"></th>
                                        </tr>
                                        <tr>
                                            <th class="align-middle">Nomor buku</th>
                                            <th><input name="number" type="number" class="form-control" id="book-number"></th>
                                        </tr>
                                        <tr>
                                            <th class="align-middle">Kode buku</th>
                                            <th><input name="code" type="text" class="form-control" id="book-code"></th>
                                        </tr>
                                        <tr>
                                            <th class="align-middle">Halaman Pendahuluan</th>
                                            <th><input name="prepage" type="number" class="form-control" id="book-prepage"></th>
                                        </tr>
                                        <tr>
                                            <th class="align-middle">Cover buku</th>
                                            <th>
                                                <div id="book-cover-image"></div><br>
                                                <input name="cover" type="file" class="form-control" id="book-cover-image-input">
                                            </th>
                                        </tr>

                                        <tr>
                                            <th class="align-middle">Suara cover</th>
                                            <th>
                                                <div id="book-cover-sound"></div><br>
                                                <input name="cover_sound" type="file" class="form-control" id="book-cover-sound-input">
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="2">
                                                <div class="row w-100 m-0">
                                                    <div class="col-sm-8 align-middle">
                                                        <div id="notif-update-book" class="text-break m-0"></div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <button id="form-submit" type="submit" class="float-right btn btn-success" data-loading-text="Updating...">Update</button>
                                                    </div>
                                                </div>
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </form>
                                <br>
                                <h2 class="card-title mb-2">Daftar halaman</h2>
                                <table class="table dt-responsive w-200">
                                    <thead >
                                    <tr>
                                        <th style='width: 1px;text-align: center'>No Halaman</th>
                                        <th style='width: 1px;text-align: center'>Gambar Halaman</th>
                                        <th style='width: 1px;text-align: center'></th>
                                        <th style='width: 1px;text-align: center'></th>
                                    </tr>
                                    </thead>

                                    <tbody id="body-table">

                                    </tbody>
                                </table>
                            </div>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
                <div class="col-12 box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h2 id="book-title" class="card-title mb-2">Tambah Halaman Baru</h2>
                            <form id="form-create-page">
                                <table class="table dt-responsive w-200">
                                    <tbody id="body-detail-table">
                                        <tr>
                                            <th class="align-middle">Nomor Halaman</th>
                                            <th><input name="page_number" type="number" class="form-control" id="page-number"></th>
                                        </tr>
                                        <tr>
                                            <th class="align-middle">Gambar halaman</th>
                                            <th>
                                                <input name="image_ind" type="file" class="form-control" id="page-image-input-ind">
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="3"></th>
                                        </tr>
                                        <tr><th><b>Audio</b></th></tr>
                                        <tr>
                                            <th class="align-middle">Indonesia</th>
                                            <th>
                                                <input name="sound_ind" type="file" class="form-control" id="page-sound-input-ind">
                                            </th>
                                        </tr>
                                        <tr>
                                            <th class="align-middle">English</th>
                                            <th>
                                                <input name="sound_eng" type="file" class="form-control" id="page-sound-input-eng">
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="2">
                                                <div class="row w-100 m-0">
                                                    <div class="col-sm-8 align-middle">
                                                        <div id="notif-create-page" class="text-break m-0"></div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <button id="form-submit-create" type="submit" class="float-right btn btn-success">Tambah</button>
                                                    </div>
                                                </div>
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script>
        var bookDetail = null
        $( document ).ready(function() {
            getBookDetail();
            initFormCreatePage();
            initFormUpdateBook();
        });

        function implBooksDetail(data){
            $("#book-title").val(data.data.name);
            $("#book-number").val(data.data.number);
            $("#book-code").val(data.data.code);
            $("#book-prepage").val(data.data.prepage);

            var openImageCover = "openImageModal(\"{{url("")}}/"+data.data.cover_url+"\");";

            $("#book-cover-image").html("<button type='button' class='btn btn-xs btn-primary' onClick='"+openImageCover+"'>Lihat</td>");
            $("#book-cover-sound").html(getAudioPlayer(data.data.cover_sound_url));
        }
        function getBookDetail(){
            const contentBook = $("#content-book");
            const loadingBook = $("#loading-book");
            contentBook.hide();
            loadingBook.show();
            $.get("/api/psbs/book/detail/"+{{$book_id}}).done(function (data, sts) {
                bookDetail = data.data;
                const table = $("#body-table");
                table.html("");
                implBooksDetail(data);
                loadingBook.hide();
                contentBook.show();
                if(data.data.pages.length === 0){
                    $("#body-table").append("<tr><td colspan='4' style='width: 1px;text-align: center'>Belum ada data</td></tr>")
                }
                data.data.pages.forEach(function (item, index) {
                    let openImageInd = "openImageModal(\"{{url("")}}/" + item.image_ind_url + "\");";
                    let openImageEng = "openImageModal(\"{{url("")}}/" + item.image_eng_url + "\");";

                    table.append(
                        "<tr onclick=\"\">"+
                            "<td style='width: 1px;text-align: center'>"+item.page_number+"</td>"+
                            "<td style='width: 1px;text-align: center'><button class='btn btn-xs btn-primary' onClick='"+openImageInd+"'>Lihat</button></td>"+
                            "<td style='width: 1px;text-align: center'><a class='btn btn-xs btn-success' href='/psbs/book/page/"+item.id+"'>Edit</a></td>"+
                            "<td style='width: 1px;text-align: center'><button onclick='deletePage(this,"+item.id+")' class='btn btn-xs btn-danger'>Hapus</button></td>"+

                        "</tr>"
                    );
                });

            });
        }
        function initFormUpdateBook(){
            const formUpdateBook = $("#form-update-book");
            formUpdateBook.on('submit', function (e) {
                e.preventDefault();
                const button = $('#form-submit');
                const notifUpdateBook = $('#notif-update-book');
                notifUpdateBook.html("");
                button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                button.attr("disabled", true);
                $.ajax({
                    type: 'POST',
                    url: '/api/admin/psbs/book/update/'+bookDetail.id,
                    data: new FormData(this),
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                        button.attr("disabled", true);
                    },
                    success: function (data) {
                        formUpdateBook.trigger('reset');
                        getBookDetail();
                        notifUpdateBook.html("<p class='text-success m-1'>Book updated successfully</p>")
                    },
                    error: function (data) {
                        const errors = data.responseJSON.errors;
                        Object.keys(data.responseJSON.errors).forEach((item,index)=>{
                            notifUpdateBook.append("<p class='text-danger m-1'>"+errors[item][0]+"</p>")
                        })
                    },
                    complete: function (data) {
                        // implPageDetail(data);
                        button.html("Update");
                        button.removeAttr("disabled");
                    }
                })

                return false;
            });
        }
        function initFormCreatePage() {
            const formCreatePage = $("#form-create-page");
            formCreatePage.on('submit', function (e) {
                e.preventDefault();
                const notifCreatePage = $('#notif-create-page');
                notifCreatePage.html("");
                const button = $('#form-submit-create');
                const data = new FormData(this);
                data.set("book_id", {{$book_id}});
                $.ajax({
                    type: 'POST',
                    url: '/api/admin/psbs/book/page/create',
                    data: data,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                        button.attr("disabled", true);
                    },
                    success: function (data) {
                        formCreatePage.trigger('reset');
                        getBookDetail();
                        notifCreatePage.html("<p class='text-success m-1'>Page created successfully</p>")
                    },
                    error: function (data) {
                        const errors = data.responseJSON.errors;
                        Object.keys(data.responseJSON.errors).forEach((item,index)=>{
                            notifCreatePage.append("<p class='text-danger m-1'>"+errors[item][0]+"</p>")

                        })
                    },
                    complete: function (data) {
                        // implPageDetail(data);
                        button.html("Tambah");
                        button.removeAttr("disabled");
                    }
                })
            });
        }
        function deletePage(btn, id){
            if(confirm("Anda yakin ingin menghapus ini?")) {
                var button = $(btn);
                $.ajax({
                    type: 'DELETE',
                    url: '/api/admin/psbs/book/page/delete/' + id,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                        button.attr("disabled", true);
                    },
                    success: function (data) {
                        getBookDetail();
                    },
                    error: function (data) {
                        const errors = data.responseJSON.message;
                        alert(errors);
                        // implPageDetail(data);
                        button.html("Hapus");
                        button.removeAttr("disabled");
                    },
                    complete: function (data) {

                    }
                })
            }
        }
    </script>
@endsection
