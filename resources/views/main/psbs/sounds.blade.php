@extends('template.master')
@section('konten')
<div class="content-wrapper">
    <div class="data-table-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h2 id="book-title" class="card-title mb-2"></h2>
                            <div class="d-flex justify-content-center">
                                <div id="loading-page"  class="spinner-border text-primary m-5" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>
                            <div id="content-page">
                                <form id="form-update-page">
                                    <table class="table dt-responsive w-200">
                                        <tbody id="body-detail-table">
                                            <tr>
                                                <th class="align-middle">Nomor Halaman</th>
                                                <th colspan="2"><input name="page_number" type="number" class="form-control" id="page-number"></th>
                                            </tr>
                                            <tr>
                                                <th class="align-middle">Gambar Halaman</th>
                                                <th>
                                                    <input name="image_ind" type="file" class="form-control" id="page-image-input-ind">
                                                </th>
                                                <th>
                                                    <div id="page-image-ind"></div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th colspan="3"></th>
                                            </tr>

                                            <tr>
                                                <th class="align-middle" colspan="3"><b>Audio</b></th>
                                            </tr>
                                            <tr>
                                                <th class="align-middle">Indonesia</th>
                                                <th>
                                                    <input name="sound_ind" type="file" class="form-control" id="page-sound-input-ind">
                                                </th>
                                                <th>
                                                    <div id="page-sound-ind"></div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th class="align-middle">English</th>
                                                <th>
                                                    <input name="sound_eng" type="file" class="form-control" id="page-sound-input-eng">
                                                </th>
                                                <th>
                                                    <div id="page-sound-eng"></div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th colspan="3">
                                                    <div class="row w-100 m-0">
                                                        <div class="col-sm-8 align-middle">
                                                            <div id="notif-update-page" class="text-break m-0"></div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <button id="form-submit" type="submit" class="float-right btn btn-success">Update</button>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
        </div>
    </div>
</div>
<script>
    var pageDetail = null;
    var currentSoundId = null;
    $( document ).ready(function() {
        getPageDetail();
        initFormUpdatePage();

    });
    function getPageDetail(){
        const contentPage = $("#content-page");
        const loadingPage = $("#loading-page");
        contentPage.hide();
        loadingPage.show();

        $.get("/api/psbs/book/page/detail/"+{{$page_id}}).done(function (data, sts) {
            $("#body-table-2").html("");
            $("#body-table").html("");
            $("#book-title").html(data.data.book.name);
            contentPage.show();
            loadingPage.hide();
            pageDetail = data.data;
            implPageDetail(data);
        });
    }

    function initFormUpdatePage(){
        const formUpdatePage = $("#form-update-page");
        formUpdatePage.on('submit', function (e) {
            e.preventDefault();
            const button = $('#form-submit');
            const notifUpdatePage = $('#notif-update-page');
            notifUpdatePage.html("");
            button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
            button.attr("disabled", true);
            $.ajax({
                type: 'POST',
                url: '/api/admin/psbs/book/page/update/'+pageDetail.id,
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    button.html("<i class='fa fa-spinner fa-spin '></i> Loading ");
                    button.attr("disabled", true);
                },
                success: function (data) {
                    formUpdatePage.trigger('reset');
                    getPageDetail();
                    notifUpdatePage.html("<p class='text-success m-1'>Page updated successfully</p>")

                },
                error: function (data) {
                    const errors = data.responseJSON.errors;
                    Object.keys(data.responseJSON.errors).forEach((item,index)=>{
                        notifUpdatePage.append("<p class='text-danger m-1'>"+errors[item][0]+"</p>")

                    })
                },
                complete: function (data) {
                    // implPageDetail(data);
                    button.html("Update");
                    button.removeAttr("disabled");
                }
            })

            return false;
        });
    }

    function implPageDetail(data){
        $("#page-number").val(data.data.page_number);
        let openImageCoverInd = "openImageModal(\"{{url("")}}/" + data.data.image_ind_url + "\");";
        let openImageCoverEng = "openImageModal(\"{{url("")}}/" + data.data.image_eng_url + "\");";
        $("#page-image-ind").html("<button type='button' class='btn btn-xs btn-primary' onClick='"+openImageCoverInd+"'>Lihat</td>");
        $("#page-image-eng").html("<button type='button' class='btn btn-xs btn-primary' onClick='"+openImageCoverEng+"'>Lihat</td>");
        $("#page-sound-ind").html(getAudioPlayer(data.data.sound_ind_url));
        $("#page-sound-eng").html(getAudioPlayer(data.data.sound_eng_url));
    }


</script>
@endsection
