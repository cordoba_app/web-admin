<!DOCTYPE html>
<html lang="en">
@include('template.head')
<body>
<div id="preloader-area">
        <div class="lds-ripple">
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="main-container-wrapper">
        @include('template.navbar')
        <div class="container-fluid page-body-wrapper">
            @include('template.sidebar')
            <div class="main-panel">
                @yield('konten')
            </div>
        </div>
    </div>
    
    @include('template.script')
    
    <script>
        $( document ).ready(function() {
            console.log( "ready!" );
            $('#preloader-area').fadeOut('slow', function () {
                $(this).remove();
            });
        });
    </script>
</body>
</html>