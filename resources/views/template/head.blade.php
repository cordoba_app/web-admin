<head>
<meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Required meta tags -->
    <title>Home</title>

    <!-- Favicon -->
    <link rel="icon" href="assets/template/img/core-img/favicon.png">

    <!-- Plugins css -->
    <link rel="stylesheet" href="{{asset('assets/template/css/default-assets/mini-event-calendar.min.css')}} ">
    <!-- Plugins Js -->
    <script src="{{ asset('assets/template/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/bundle.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

    <!-- Active JS -->
    <script src="{{ asset('assets/template/js/default-assets/active.js') }}"></script>
    <!-- Master Stylesheet CSS -->
    <link rel="stylesheet" href="{{asset('assets/template/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/template/css/default-assets/modal.css') }}">
    <link rel="stylesheet" href="{{asset('assets/template/css/default-assets/datatables.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('assets/template/css/default-assets/responsive.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('assets/template/css/default-assets/buttons.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('assets/template/css/default-assets/select.bootstrap4.css')}}">
    <style>
        .btn.btn-xs {
            padding: 4px 16px !important;
        }
    </style>
    <script>
        var days = ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'];
        var month = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'];
        $( document ).ready(function() {
            console.log(localStorage.getItem('token'));
            $.ajaxSetup({
                headers: {
                    'Authorization': "Bearer " + localStorage.getItem('token')
                }
            });
        });
        var currentAudio = new Audio();

        function getAudioPlayer(url, id="def-audio"){
            var fullUrl = "{{url("")}}/"+url;
            return "<audio id='"+id+"' controls> <source src='"+fullUrl+"' type='audio/mpeg'> </audio>"
        }

        function playAudio(url) {
            currentAudio.pause();
            currentAudio = new Audio(url);
            currentAudio.play();
        }

        function openImageModal(imageUrl){
            $('#imagepreview').attr('src', imageUrl); // here asign the image to the modal when the user click the enlarge link
            $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
        }

        $.get("/api/admin/check").fail(function (){
            localStorage.clear();
            window.location.href = "/";
        });

        const editIcon = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit link-icon"> <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path> <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg>';


    </script>

</head>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                <img src="" id="imagepreview" style="width: 100%;" >
            </div>

        </div>
    </div>
</div>

<script>
    $.ajaxSetup({
        headers:{
            'Authorization': "Bearer "
        }
    });

</script>
