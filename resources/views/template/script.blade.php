
    <script src="{{ asset('assets/template/js/jquery.min.js') }}"></script>
    <!-- <script src="js/jquery.min.js"></script> -->
<!-- Plugins Js -->

    <script src="{{ asset('assets/template/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/bundle.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/fullscreen.js') }}"></script>

    <!-- Active JS -->
    <script src="{{ asset('assets/template/js/canvas.js') }}"></script>
    <script src="{{ asset('assets/template/js/collapse.js') }}"></script>
    <script src="{{ asset('assets/template/js/settings.js') }}"></script>
    <script src="{{ asset('assets/template/js/template.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/active.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/setting.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/fullscreen.js') }}"></script>

    <!-- Inject JS -->
    <script src="{{ asset('assets/template/js/default-assets/mini-event-calendar.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/mini-calendar-active.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/apexchart.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/dashboard-active.js') }}"></script>

    <script src="{{ asset('assets/template/js/default-assets/jquery.datatables.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/datatables.bootstrap4.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/datatable-responsive.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/datatable-button.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/button.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/button.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/button.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/button.print.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/datatables.keytable.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/datatables.select.min.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/demo.datatable-init.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/modal-classes.js') }}"></script>
    <script src="{{ asset('assets/template/js/default-assets/modaleffects.js') }}"></script>

