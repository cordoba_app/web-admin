
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">

        <li class="nav-item">
            <div>
                <a class="nav-link" href="#aaci"  data-toggle="collapse" data-target="#cd-aaci">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit link-icon">
                        <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                        <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                    </svg>
                    <span class="menu-title">AACI</span>
                    <i class="ti-angle-right"></i>
                </a>
                <div class="collapse" id="cd-aaci">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item"><a class="nav-link" href="/kids">Buku AACI</a></li>
                    </ul>
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item"><a class="nav-link" href="/kids/bonus">Ular Tangga</a></li>
                    </ul>
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item"><a class="nav-link" href="/kids/setting">Pengaturan</a></li>
                    </ul>
                </div>
            </div>
        </li>

        <li class="nav-item">
            <div>
                <a class="nav-link" href="#psbs"  data-toggle="collapse" data-target="#cd-psbs">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit link-icon">
                        <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                        <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                    </svg>
                    <span class="menu-title">PS-BS</span>
                    <i class="ti-angle-right"></i>
                </a>
                <div class="collapse" id="cd-psbs">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item"><a class="nav-link" href="/psbs">Buku PS-BS</a></li>
                    </ul>
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item"><a class="nav-link" href="/psbs/setting">Pengaturan</a></li>
                    </ul>
                </div>
            </div>
        </li>

<!--         <li class="nav-item">
            <a class="nav-link" href="/admin/user">
                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users link-icon">
                    <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                    <circle cx="9" cy="7" r="4"></circle>
                    <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                    <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                </svg>
                <span class="menu-title">Members</span>
            </a>
        </li> -->

    </ul>
</nav>
<script>
    $( document ).ready(function() {

    });
    function logout() {
        if(confirm("Kamu akan dialihkan ke halaman login.")){
            localStorage.clear();
            window.location.href = "/";
        }
    }
</script>
