@echo off 
setlocal enableDelayedExpansion 


goto test

:test
cls
echo running loop test
set indexPageOffset=2

echo page_id,type,number,sound_url>"test.csv"

for /l %%i in (1,1,6) do (
set MYDIR="%cd%/%%i/sound"
set currentPageOffset=!indexPageOffset!
for /F %%x in ('dir /B/D !MYDIR!') do (
	set filename=%%x
	echo %%x|find "hal" >nul
	if not errorlevel 1 (

		set filename=!filename:Bk_%%i_hal_=!
		set filename=!filename:.WAV=!

		for /f "tokens=1,2 delims=_" %%a in ("!filename!") do (
  			set page=%%a
			set symbol=%%b
		)
		set type=2
		set /A page_id=!page!+!indexPageOffset!
		if not !symbol!==a if not !symbol!==b if not !symbol!==c set type=1
		set number=!symbol!
		if !symbol!==a set number=1
		if !symbol!==b set number=2
		if !symbol!==c set number=3
		if !currentPageOffset! lss !page_id! set currentPageOffset=!page_id!
		echo  ,!page_id!,!type!,!number!,upload/books/pages/%%i/sound/Bk_%%i_hal_!page!_!symbol!.WAV
	)
)>>"test.csv"
set indexPageOffset=!currentPageOffset!+2
)
echo Done
pause