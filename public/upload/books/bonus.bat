@echo off 
setlocal enableDelayedExpansion 


goto test

:test
cls
echo running loop test

set MYDIR="%cd%/bonus"
for /F %%x in ('dir /B/D !MYDIR!') do (
	set filename=%%x
	set filename=!filename:UT_=!
	set filename=!filename:.WAV=!

	for /f "tokens=1,2,3 delims=_" %%a in ("!filename!") do (
  		set order=%%a
		set type=%%b
		set color=%%c
	)

	if !type!==J (
		echo  ,upload/books/bonus/UT_!order!_J_!color!.WAV,upload/books/bonus/UT_!order!_T_!color!.WAV,!order!
	)
	
)>>"bonus.csv"


echo Done
pause