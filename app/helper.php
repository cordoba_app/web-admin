<?php

use Illuminate\Support\Facades\Validator;

if(!function_exists("do_response")){
    function do_response($content = '',$message = "Success", $status = 200 ){
        $response = ["status" => $status, "message" => $message, "data" => $content];
        return response($response, $status);
    }
}

if(!function_exists("validate")){
    function validate(array $data, array $rules, array $messages = [], array $customAttributes = []){
        $validator = Validator::make($data, $rules, $messages, $customAttributes);
        if($validator->fails()){
            return do_response($validator->errors(), "Error", 400);
        } else {
            return null;
        }
    }
}

if(!function_exists("do_error")){
    function do_error($message = "Success", $status = 400 ){
        $response = ["status" => $status, "message" => $message, "data" => null];
        return response($response, $status);
    }
}
