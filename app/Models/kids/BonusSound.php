<?php


namespace App\Models\kids;


use Illuminate\Database\Eloquent\Model;

class BonusSound extends Model
{
    protected $table = 'kids_tbl_bonus_sound';
    protected $fillable = [
        'question_sound_url',
        'answer_sound_url',
        'orders',
        'color'
    ];

}
