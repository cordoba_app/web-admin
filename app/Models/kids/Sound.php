<?php


namespace App\Models\kids;


use Illuminate\Database\Eloquent\Model;

class Sound extends Model
{
    protected $table = 'kids_tbl_sound';
    protected $fillable = [
        'page_id',
        'sound_url',
        'type',
        'symbol',
        'orders'
    ];

}
