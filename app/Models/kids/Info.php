<?php


namespace App\Models\kids;


use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    protected $table = 'kids_master_information';
    protected $fillable = [
        'about',
        'guide'
    ];
    public $timestamps = false;

}
