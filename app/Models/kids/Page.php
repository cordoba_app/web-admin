<?php


namespace App\Models\kids;


use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'kids_tbl_page';
    protected $fillable = [
        'book_id',
        'page_number',
        'image_url',
    ];
    public function sounds()
    {
        return $this->hasMany(Sound::class, 'page_id', 'id')->orderBy("orders");
    }
    public function book()
    {
        return $this->hasOne(Book::class, 'id', 'book_id');
    }

}
