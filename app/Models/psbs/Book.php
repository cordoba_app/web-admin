<?php


namespace App\Models\psbs;


use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'psbs_master_book';
    protected $fillable = [
        'name',
        'cover_url',
        'cover_sound_url',
        'number',
        'code',
        'prepage'
    ];

    public function pages()
    {
        return $this->hasMany(Page::class, 'book_id', 'id')->orderBy('page_number');
    }
}
