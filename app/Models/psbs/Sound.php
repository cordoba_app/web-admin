<?php


namespace App\Models\psbs;


use Illuminate\Database\Eloquent\Model;

class Sound extends Model
{
    protected $table = 'psbs_tbl_sound';
    protected $fillable = [
        'page_id',
        'sound_ind_url',
        'sound_eng_url',
        'type',
        'symbol',
        'orders'
    ];

}
