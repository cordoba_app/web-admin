<?php


namespace App\Models\psbs;


use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'psbs_tbl_page';
    protected $fillable = [
        'book_id',
        'page_number',
        'image_ind_url',
        'image_eng_url',
        'sound_ind_url',
        'sound_eng_url'
    ];

    public function book()
    {
        return $this->hasOne(Book::class, 'id', 'book_id');
    }

}
