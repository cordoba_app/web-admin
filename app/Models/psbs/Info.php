<?php


namespace App\Models\psbs;


use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    protected $table = 'psbs_master_information';
    protected $fillable = [
        'about',
        'guide'
    ];
    public $timestamps = false;

}
