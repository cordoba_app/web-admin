<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleLevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$levels)
    {
        if(!Auth::check()){
            return do_error("Kamu tidak memiliki akses", 401);
        }
        $user = Auth::user();

        foreach($levels as $level) {
            // Check if user has the role This check will depend on how your roles are set up
            if($user->role->level <= $level)
                return $next($request);
        }
        return do_error("Kamu tidak memiliki akses", 401);
    }
}
