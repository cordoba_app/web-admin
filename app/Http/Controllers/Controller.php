<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Faker\Provider\Uuid;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Str;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function me(){
         return do_response(User::first());
    }

    function check(){
        return Auth()->user();
    }

    function getAllRoles(){
        return do_response(Role::all());
    }
    function changeUsername(Request $request){
        $this->validate($request,[
            'username' => 'required|unique:tbl_user,username',
        ]);
        $data = User::first();
        $data['username'] = $request['username'];
        return do_response($data->save());

    }
    function changeUserPass(Request $request){
        $this->validate($request,[
            'password' => 'min:6|required_with:password-confirmation|same:password-confirmation',
            'password-confirmation'
        ]);
        $data = User::first();

        $data["password"] = bcrypt($request["password"]);
        return do_response($data->save());

    }
    public function uploadFile(Request $request, $paramName, $dir){
        $this->validate($request, [
            $paramName => 'required'
        ]);

        $file = $request->file($paramName);
        if($file) {
            $target_dir = 'upload/' . $dir;

            // upload file
            $name = $target_dir."/".md5("".time()."-".Str::uuid()) .".". ($file->getClientOriginalExtension());
            if ($file->move($target_dir, $name)) return $name;
        }
        return null;
    }
}




