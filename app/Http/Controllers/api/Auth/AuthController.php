<?php
namespace App\Http\Controllers\api\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller {


    public function cek(){
        return Hash::make('admin');
    }

    public function login(Request $request)
    {
        $validator = validate($request->all(), [
                'username' => 'required',
                'password' => 'required',
            ]);

        if($validator != null) return $validator;

        $credentials = $request->only('username', 'password');
        if(Auth::validate($credentials)){
            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return do_response(['error' => "Token tidak valid"], null,400);
                }
            } catch (JWTException $e) {
                return do_response(['error' => $e->getMessage()], null,500);
            }
            $user = auth()->user();
            return do_response(compact('token', 'user'));
        } else {
            return do_response(null, "Username atau password salah", 401);
        }

    }

    public function me(){
        return do_response(auth()->user());
    }

    public function logout(){

    }

}


