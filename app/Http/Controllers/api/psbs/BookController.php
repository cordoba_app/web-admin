<?php
namespace App\Http\Controllers\api\psbs;
use App\Http\Controllers\Controller;
use App\Models\psbs\Book;
use App\Models\psbs\Page;
use App\Models\psbs\Sound;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class BookController extends Controller {

//    BOOK
////    CREATE
    function createBook(Request $request){
        $data = $request->all();
        $this->validate($request, [
            'name' => 'required',
            'number' => 'required|unique:psbs_master_book,number',
            'code' => 'required|unique:psbs_master_book,code',
        ]);
        $image_url = $this->uploadFile($request, "cover", "psbs/cover");
        $sound_url = $this->uploadFile($request, "cover_sound", "psbs/sound");
        $data['cover_url'] = $image_url;
        $data['cover_sound_url'] = $sound_url;
        return do_response(Book::create($data));
    }

////    READ
    function getAllBooks(){
        return do_response(Book::orderBy("number")->get());
    }

    function getBookDetail($id){
        return do_response(Book::with('pages')->find($id));
    }

    function getBookByCode($code){
        $data = Book::with('pages')->where("code", $code)->first();
        if($data){
            return do_response($data);
        } else {
            return do_error("Data tidak ditemukan");
        }
    }

////    EDIT
    function updateBook(Request $request, $id){
        $data = Book::find($id);
        $newData = $request->all();
        $toValidate = ['name' => 'required'];
        if($data['number'] != $newData['number']){
            $toValidate['number'] = 'required|unique:psbs_master_book,number';
        }
        if($data['code'] != $newData['code']){
            $toValidate['code'] = 'required|unique:psbs_master_book,code';
        }
        $this->validate($request, $toValidate);
        foreach ($newData as $key=>$value){
            if($value && $value!=$data[$key]) $data[$key] = $value;
        }

        $tempFile1 = $data->image_url;
        $tempFile2 = $data->sound_url;
        $isUpdateCoverImage = isset($request['cover']);
        $isUpdateCoverSound = isset($request['cover_sound']);
        if($isUpdateCoverImage) {
            $image_url = $this->uploadFile($request, "cover", "psbs/cover");
            $data['cover_url'] = $image_url;
            unset($data['cover']);
        }
        if($isUpdateCoverSound) {
            $sound_url = $this->uploadFile($request, "cover_sound", "psbs/sound");
            $data['cover_sound_url'] = $sound_url;
            unset($data['cover_sound']);
        }
        $res = $data->save();

        if($res){
            try {
                if($isUpdateCoverImage && $isUpdateCoverSound){
                    File::delete([$tempFile1, $tempFile2]);
                } else if(!$isUpdateCoverImage && $isUpdateCoverSound) {
                    File::delete($tempFile2);
                } else if($isUpdateCoverImage && !$isUpdateCoverSound) {
                    File::delete($tempFile1);
                }
            } catch (\Exception $e){

            }
        }
        return do_response($data);
    }

////    DELETE
    function deleteBook($id){
        $data = Book::find($id);
        if($data) {
            File::delete([$data->cover_url, $data->cover_sound_url]);

            return do_response($data->delete());
        }
        return do_error("Data tidak ditemukan", 404);

    }

//  END OF BOOK
//  PAGE
////    CREATE
    function createPage(Request $request){
        $data = $request->all();
        $this->validate($request, [
            'book_id' => 'required',
            'page_number' => 'required|unique:psbs_tbl_page,page_number',
        ]);
        $image_ind_url = $this->uploadFile($request, "image_ind", "psbs/pages/".$data['book_id']);
        $data['image_ind_url'] = $image_ind_url;
        unset($data['image_ind']);

        if(isset($request["image_eng"])) {
            $image_eng_url = $this->uploadFile($request, "image_eng", "psbs/pages/".$data['book_id']);
            $data['image_eng_url'] = $image_eng_url;
            unset($data['image_eng']);
        }

        $sound_url = $this->uploadFile($request, "sound_ind", "psbs/pages/".$data['book_id']."/sound/ind");
        $data['sound_ind_url'] = $sound_url;
        unset($data['sound_ind']);


        if(isset($request['sound_eng'])) {
            $sound_url = $this->uploadFile($request, "sound_eng", "psbs/pages/".$data['book_id']."/sound/eng");
            $data['sound_eng_url'] = $sound_url;
            unset($data['sound_eng']);
        }


        return do_response(Page::create($data));
    }
////    READ
    function getPageDetail($id){
        $data = Page::with('book')->find($id);
        if($data){
            return do_response($data);
        } else {
            return do_error("Data tidak ditemukan");
        }
    }
////    EDIT
    function updatePage(Request $request, $id){
        $data = Page::find($id);
        $newData = $request->all();
        $toValidate = [];
        if($data['page_number'] != $newData['page_number']){
            $toValidate['page_number'] = 'required|unique:psbs_tbl_page,page_number';
        }
        $this->validate($request, $toValidate);

        foreach ($newData as $key=>$value){
            if($value && $value!=$data[$key]) $data[$key] = $value;
        }
        if($data['page_number'] != $newData['page_number']){
            $this->validate($request, [
                'page_number' => 'unique:psbs_tbl_page,page_number'
            ]);
        }
        $tempFile = [];
        $isUpdateImageInd = isset($request['image_ind']);
        $isUpdateImageEng = isset($request['image_eng']);
        if($isUpdateImageInd) {
            $tempFile[] = $data->image_ind_url;
            $image_url = $this->uploadFile($request, "image_ind", "psbs/pages/" . $id);
            $data['image_ind_url'] = $image_url;
            unset($data['image_ind']);
        }

        if($isUpdateImageEng) {
            $tempFile[] = $data->image_eng_url;
            $image_url = $this->uploadFile($request, "image_eng", "psbs/pages/" . $id);
            $data['image_eng_url'] = $image_url;
            unset($data['image_eng']);
        }

        $tempFile2 = [];
        if(isset($request['sound_ind'])) {
            $tempFile2[] = $data->sound_ind_url;
            $sound_url = $this->uploadFile($request, "sound_ind", "psbs/pages/".$data['book_id']."/sound/ind");
            $data['sound_ind_url'] = $sound_url;
            unset($data['sound_ind']);
        }

        if(isset($request['sound_eng'])) {
            $tempFile2[] = $data->sound_eng_url;
            $sound_url = $this->uploadFile($request, "sound_eng", "psbs/pages/".$data['book_id']."/sound/eng");
            $data['sound_eng_url'] = $sound_url;
            unset($data['sound_eng']);
        }



        $res = $data->save();
        if($res){
            try {
                File::delete($tempFile);
            } catch (\Exception $e){

            }

            try {
                File::delete($tempFile2);
            } catch (\Exception $e){

            }
        }
        return do_response($data);
    }

////    DELETE
    function deletePage($id){
        $data = Page::find($id);
        if($data) {
            File::delete([$data->image_ind_url, $data->image_eng_url, $data->sound_ind_url, $data->sound_eng_url]);
            return do_response($data->delete());
        }
        return do_error("Data tidak ditemukan", 404);
    }
//END OF PAGE

//  PAGE
////    CREATE
    function createSound(Request $request){
        $this->validate($request, [
            'page_id' => 'required',
            'sound_ind' => 'required',
            'orders' => 'required'
        ]);
        $data = $request->all();

        $sound_ind_url = $this->uploadFile($request, "sound_ind", "psbs/pages/".$data['page_id']."/sound/ind");
        $data['sound_ind_url'] = $sound_ind_url;
        if(isset($request['sound_eng'])) {
            $sound_eng_url = $this->uploadFile($request, "sound_eng", "psbs/pages/".$data['page_id']."/sound/eng");
            $data['sound_eng_url'] = $sound_eng_url;
        }
        unset($data['sound_ind']);
        unset($data['sound_eng']);


        return do_response(Sound::create($data));
    }

////    EDIT
    function updateSound(Request $request, $id){
        $data = Sound::find($id);
        $newData = $request->all();

        foreach ($newData as $key=>$value){
            if($value) $data[$key] = $value;
        }
        $tempFile = [];
        if(isset($request['sound_ind'])) {
            $tempFile[] = $data->sound_ind_url;
            $sound_url = $this->uploadFile($request, "sound_ind", "psbs/pages/".$data['page_id']."/sound/ind");
            $data['sound_ind_url'] = $sound_url;
            unset($data['sound_ind']);
        }

        if(isset($request['sound_eng'])) {
            $tempFile[] = $data->sound_eng_url;
            $sound_url = $this->uploadFile($request, "sound_eng", "psbs/pages/".$data['page_id']."/sound/eng");
            $data['sound_eng_url'] = $sound_url;
            unset($data['sound_eng']);
        }


        $res = $data->save();
        if($res){
            try {
                File::delete($tempFile);
            } catch (\Exception $e){

            }
        }
        return do_response($res);
    }

////    DELETE
    function deleteSound($id){
        $data = Sound::find($id);
        if($data) {
            File::delete($data->sound_url);
            return do_response($data->delete());
        }
        return do_error("Data tidak ditemukan", 404);
    }
//END OF PAGE

}
