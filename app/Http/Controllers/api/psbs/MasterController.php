<?php
namespace App\Http\Controllers\api\psbs;
use App\Http\Controllers\Controller;
use App\Models\psbs\Info;
use Illuminate\Http\Request;

class MasterController extends Controller {
    function getInformation(){
        return do_response(Info::first());
    }

    function setAbout(Request $request){
        $this->validate($request, [
            'about' => 'required',
        ]);
        $data = Info::first();
        if($data){
            $data['about'] = $request->about;
        }
        $data->save();
        return do_response($data);
    }

    function setGuide(Request $request){
        $this->validate($request, [
            'guide' => 'required',
        ]);
        $data = Info::first();
        if($data){
            $data['guide'] = $request->guide;
        }
        $data->save();
        return do_response($data);
    }
}
