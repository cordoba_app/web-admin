<?php
namespace App\Http\Controllers\api\kids;
use App\Http\Controllers\Controller;
use App\Models\kids\BonusSound;
use App\Models\kids\Book;
use App\Models\kids\Page;
use App\Models\kids\Sound;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class BookController extends Controller {

//    BOOK
////    CREATE
    function createBook(Request $request){
        $data = $request->all();
        $this->validate($request, [
            'name' => 'required',
            'number' => 'required|unique:kids_master_book,number',
            'code' => 'required|unique:kids_master_book,code',
        ]);
        $image_url = $this->uploadFile($request, "cover", "books/cover");
        $sound_url = $this->uploadFile($request, "cover_sound", "books/sound");
        $data['cover_url'] = $image_url;
        $data['cover_sound_url'] = $sound_url;
        return do_response(Book::create($data));
    }

////    READ
    function getAllBooks(){
        return do_response(Book::orderBy("number")->get());
    }

    function getBookDetail($id){
        return do_response(Book::with('pages')->find($id));
    }

    function getBookByCode($code){
        $data = Book::with('pages')->where("code", $code)->first();
        if($data){
            return do_response($data);
        } else {
            return do_error("Data tidak ditemukan");
        }
    }

////    EDIT
    function updateBook(Request $request, $id){
        $data = Book::find($id);
        $newData = $request->all();
        $toValidate = ['name' => 'required'];
        if($data['number'] != $newData['number']){
            $toValidate['number'] = 'required|unique:kids_master_book,number';
        }
        if($data['code'] != $newData['code']){
            $toValidate['code'] = 'required|unique:kids_master_book,code';
        }
        $this->validate($request, $toValidate);
        foreach ($newData as $key=>$value){
            if($value && $value!=$data[$key]) $data[$key] = $value;
        }

        $tempFile1 = $data->image_url;
        $tempFile2 = $data->sound_url;
        $isUpdateCoverImage = isset($request['cover']);
        $isUpdateCoverSound = isset($request['cover_sound']);
        if($isUpdateCoverImage) {
            $image_url = $this->uploadFile($request, "cover", "books/cover");
            $data['cover_url'] = $image_url;
            unset($data['cover']);
        }
        if($isUpdateCoverSound) {
            $sound_url = $this->uploadFile($request, "cover_sound", "books/sound");
            $data['cover_sound_url'] = $sound_url;
            unset($data['cover_sound']);
        }
        $res = $data->save();

        if($res){
            try {
                if($isUpdateCoverImage && $isUpdateCoverSound){
                    File::delete([$tempFile1, $tempFile2]);
                } else if(!$isUpdateCoverImage && $isUpdateCoverSound) {
                    File::delete($tempFile2);
                } else if($isUpdateCoverImage && !$isUpdateCoverSound) {
                    File::delete($tempFile1);
                }
            } catch (\Exception $e){

            }
        }
        return do_response($data);
    }

////    DELETE
    function deleteBook($id){
        $data = Book::find($id);
        if($data) {
            File::delete([$data->cover_url, $data->cover_sound_url]);

            return do_response($data->delete());
        }
        return do_error("Data tidak ditemukan", 404);

    }

//  END OF BOOK
//  PAGE
////    CREATE
    function createPage(Request $request){
        $data = $request->all();
        $this->validate($request, [
            'book_id' => 'required',
            'page_number' => 'required|unique:kids_tbl_page,page_number',
        ]);
        $image_url = $this->uploadFile($request, "image", "books/pages/".$data['book_id']);
        $data['image_url'] = $image_url;
        return do_response(Page::create($data));
    }
////    READ
    function getPageDetail($id){
        $data = Page::with('sounds')->with('book')->find($id);
        if($data){
            return do_response($data);
        } else {
            return do_error("Data tidak ditemukan");
        }
    }
////    EDIT
    function updatePage(Request $request, $id){
        $data = Page::find($id);
        $newData = $request->all();
        $toValidate = [];
        if($data['page_number'] != $newData['page_number']){
            $toValidate['page_number'] = 'required|unique:kids_tbl_page,page_number';
        }
        $this->validate($request, $toValidate);

        foreach ($newData as $key=>$value){
            if($value && $value!=$data[$key]) $data[$key] = $value;
        }
        if($data['page_number'] != $newData['page_number']){
            $this->validate($request, [
                'page_number' => 'unique:kids_tbl_page,page_number'
            ]);
        }
        $isUpdateImage = isset($request['image']);
        if($isUpdateImage) {
            $tempFile = $data->image_url;
            $image_url = $this->uploadFile($request, "image", "books/pages/" . $id);
            $data['image_url'] = $image_url;
            unset($data['image']);
        }
        $res = $data->save();
        if($isUpdateImage&&$res){
            try {
                File::delete($tempFile);
            } catch (\Exception $e){

            }
        }
        return do_response($data);
    }

////    DELETE
    function deletePage($id){
        $data = Page::find($id);
        if($data) {
            File::delete($data->image_url);
            return do_response($data->delete());
        }
        return do_error("Data tidak ditemukan", 404);
    }
//END OF PAGE

//  PAGE
////    CREATE
    function createSound(Request $request){
        $this->validate($request, [
            'page_id' => 'required',
            'sound' => 'required',
            'orders' => 'required'
        ]);
        $data = $request->all();

        $sound_url = $this->uploadFile($request, "sound", "books/pages/".$data['page_id']."/sound");
        $data['sound_url'] = $sound_url;
        unset($data['sound']);


        return do_response(Sound::create($data));
    }

////    EDIT
    function updateSound(Request $request, $id){
        $data = Sound::find($id);
        $newData = $request->all();

        foreach ($newData as $key=>$value){
            if($value) $data[$key] = $value;
        }
        $tempFile = $data->sound_url;
        $isUpdateSound = isset($request['sound']);
        if($isUpdateSound) {
            $sound_url = $this->uploadFile($request, "sound", "books/pages/".$data['page_id']."/sound");
            $data['sound_url'] = $sound_url;
            unset($data['sound']);
        }

        $res = $data->save();
        if($isUpdateSound && $res){
            try {
                File::delete($tempFile);
            } catch (\Exception $e){

            }
        }
        return do_response($res);
    }

////    DELETE
    function deleteSound($id){
        $data = Sound::find($id);
        if($data) {
            File::delete($data->sound_url);
            return do_response($data->delete());
        }
        return do_error("Data tidak ditemukan", 404);
    }
//END OF PAGE

//  BONUS
////    READ
    function getAllBonusSounds(){
        return do_response(BonusSound::orderBy("orders")->get());
    }
////    CREATE
    function createBonusSound(Request $request){
        $this->validate($request, [
            'question_sound' => 'required',
            'answer_sound' => 'required',
            'orders' => 'required',
            'color' => 'required'
        ]);
        $data = $request->all();

        $question_sound_url = $this->uploadFile($request, "question_sound", "books/bonus");
        $answer_sound_url = $this->uploadFile($request, "answer_sound", "books/bonus");
        $data['question_sound_url'] = $question_sound_url;
        $data['answer_sound_url'] = $answer_sound_url;
        unset($data['question_sound']);
        unset($data['answer_sound']);


        return do_response(BonusSound::create($data));
    }

////    EDIT
    function updateBonusSound(Request $request, $id){
        $data = BonusSound::find($id);
        $newData = $request->all();

        foreach ($newData as $key=>$value){
            if($value) $data[$key] = $value;
        }
        $tempFile1 = $data->question_sound_url;
        $tempFile2 = $data->answer_sound_url;
        $isUpdateQSound = isset($request['question_sound']);
        $isUpdateASound = isset($request['answer_sound']);
        if($isUpdateQSound) {
            $question_sound_url = $this->uploadFile($request, "question_sound", "books/bonus");
            $data['question_sound_url'] = $question_sound_url;
            unset($data['question_sound']);
        }

        if($isUpdateASound) {
            $answer_sound_url = $this->uploadFile($request, "answer_sound", "books/bonus");
            $data['answer_sound_url'] = $answer_sound_url;
            unset($data['answer_sound']);
        }

        $res = $data->save();
        if($res){
            try {
                if($isUpdateQSound && $isUpdateASound){
                    File::delete([$tempFile1, $tempFile2]);
                } else if(!$isUpdateQSound && $isUpdateASound) {
                    File::delete($tempFile2);
                } else if($isUpdateQSound && !$isUpdateASound) {
                    File::delete($tempFile1);
                }
            } catch (\Exception $e){

            }
        }
        return do_response($res);
    }

////    DELETE
    function deleteBonusSound($id){
        $data = BonusSound::find($id);
        if($data) {
            File::delete([$data->question_sound_url,$data->answer_sound_url]);
            return do_response($data->delete());
        }
        return do_error("Data tidak ditemukan", 404);
    }
//END OF PAGE
}
