<?php
namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller{

    public function __construct()
    {

         $this->middleware('auth:api', ['except' => ['login']]);
    }

    function getAllAdmins(Request $request){
        if($request->search == null){
            return do_response(User::all());
        } else {
            return do_response(User::where('name', 'like', '%'.$request->search.'%')->get());
        }
    }


    function createAdmin(Request $request){
        $newEmp = $request->all();
        $newEmp["password"] = bcrypt($newEmp["password"]);
        return do_response(User::create($newEmp));
    }

    function updateAdmin(Request $request, $id){
        $data = User::find($id);
        $newEmp = $request->all();
        if($newEmp["password"] != null & $newEmp["password"] != ""){
            $newEmp["password"] = bcrypt($newEmp["password"]);
        } else {
            unset($newEmp["password"]);
        }
        foreach ($newEmp as $key=>$value){
            $data[$key] = $value;
        }


        return do_response($data->save());
    }

    function removeAdmin($id){
        $data = User::find($id);
        return do_response($data->delete());
    }




}
